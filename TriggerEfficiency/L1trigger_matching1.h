//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov 12 14:09:24 2019 by ROOT version 6.08/06
// from TTree physics/physics
//////////////////////////////////////////////////////////

#ifndef L1trigger_matching1_h
#define L1trigger_matching1_h

#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TLatex.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TLegend.h"

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <iostream>

#include "/home/gsumi/RootUtils/AtlasUtils.h"
#include "/home/gsumi/RootUtils/AtlasLabels.h"

using namespace std;

class L1trigger_matching1 {
 public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  Int_t           RunNumber;
  Int_t           EventNumber;
  Int_t           lbn;
  Int_t           bcid;
  Int_t           detmask0;
  Int_t           detmask1;
  Float_t         actualIntPerXing;
  Float_t         averageIntPerXing;
  Int_t           pixelFlags;
  Int_t           sctFlags;
  Int_t           trtFlags;
  Int_t           larFlags;
  Int_t           tileFlags;
  Int_t           muonFlags;
  Int_t           fwdFlags;
  Int_t           coreFlags;
  Int_t           pixelError;
  Int_t           sctError;
  Int_t           trtError;
  Int_t           larError;
  Int_t           tileError;
  Int_t           muonError;
  Int_t           fwdError;
  Int_t           coreError;
  vector<float>   *trig_L1_mu_eta;
  vector<float>   *trig_L1_mu_phi;
  vector<string>  *trig_L1_mu_thrName;
  vector<short>   *trig_L1_mu_thrNumber;
  vector<short>   *trig_L1_mu_RoINumber;
  vector<short>   *trig_L1_mu_sectorAddress;
  vector<int>     *trig_L1_mu_firstCandidate;
  vector<int>     *trig_L1_mu_moreCandInRoI;
  vector<int>     *trig_L1_mu_moreCandInSector;
  vector<short>   *trig_L1_mu_source;
  vector<short>   *trig_L1_mu_hemisphere;
  vector<short>   *trig_L1_mu_charge;
  vector<int>     *trig_L1_mu_vetoed;
  vector<float>   *mu_pt;
  vector<float>   *mu_eta;
  vector<float>   *mu_phi;
  vector<float>   *mu_m;
  vector<int>     *mu_charge;
  vector<int>     *mu_author;
  vector<unsigned short> *mu_allAuthors;
  vector<int>     *mu_muonType;
  vector<float>   *mu_etcone20;
  vector<float>   *mu_etcone30;
  vector<float>   *mu_etcone40;
  vector<float>   *mu_ptcone20;
  vector<float>   *mu_ptcone30;
  vector<float>   *mu_ptcone40;
  vector<float>   *mu_trackfitchi2;
  vector<float>   *mu_trackfitndof;
  vector<float>   *mu_msInnerMatchChi2;
  vector<float>   *mu_msOuterMatchChi2;
  vector<int>     *mu_msInnerMatchDOF;
  vector<int>     *mu_msOuterMatchDOF;
  vector<int>     *mu_nOutliersOnTrack;
  vector<int>     *mu_nBLHits;
  vector<int>     *mu_nPixHits;
  vector<int>     *mu_nSCTHits;
  vector<int>     *mu_nTRTHits;
  vector<int>     *mu_nTRTHighTHits;
  vector<int>     *mu_nBLSharedHits;
  vector<int>     *mu_nPixSharedHits;
  vector<int>     *mu_nPixHoles;
  vector<int>     *mu_nSCTSharedHits;
  vector<int>     *mu_nSCTHoles;
  vector<int>     *mu_nTRTOutliers;
  vector<int>     *mu_nTRTHighTOutliers;
  vector<int>     *mu_nGangedPixels;
  vector<int>     *mu_nPixelDeadSensors;
  vector<int>     *mu_nSCTDeadSensors;
  vector<int>     *mu_nTRTDeadStraws;
  vector<int>     *mu_expectBLayerHit;
  vector<int>     *mu_nPrecisionLayers;
  vector<int>     *mu_nPrecisionHoleLayers;
  vector<int>     *mu_nPhiLayers;
  vector<int>     *mu_nPhiHoleLayers;
  vector<int>     *mu_nTrigEtaLayers;
  vector<int>     *mu_nTrigEtaHoleLayers;
  vector<int>     *mu_primarySector;
  vector<int>     *mu_secondarySector;
  vector<int>     *mu_nInnerSmallHits;
  vector<int>     *mu_nInnerLargeHits;
  vector<int>     *mu_nMiddleSmallHits;
  vector<int>     *mu_nMiddleLargeHits;
  vector<int>     *mu_nOuterSmallHits;
  vector<int>     *mu_nOuterLargeHits;
  vector<int>     *mu_nExtendedSmallHits;
  vector<int>     *mu_nExtendedLargeHits;
  vector<int>     *mu_nInnerSmallHoles;
  vector<int>     *mu_nInnerLargeHoles;
  vector<int>     *mu_nMiddleSmallHoles;
  vector<int>     *mu_nMiddleLargeHoles;
  vector<int>     *mu_nOuterSmallHoles;
  vector<int>     *mu_nOuterLargeHoles;
  vector<int>     *mu_nExtendedSmallHoles;
  vector<int>     *mu_nExtendedLargeHoles;
  vector<int>     *mu_nPhiLayer1Hits;
  vector<int>     *mu_nPhiLayer2Hits;
  vector<int>     *mu_nPhiLayer3Hits;
  vector<int>     *mu_nPhiLayer4Hits;
  vector<int>     *mu_nEtaLayer1Hits;
  vector<int>     *mu_nEtaLayer2Hits;
  vector<int>     *mu_nEtaLayer3Hits;
  vector<int>     *mu_nEtaLayer4Hits;
  vector<int>     *mu_nPhiLayer1Holes;
  vector<int>     *mu_nPhiLayer2Holes;
  vector<int>     *mu_nPhiLayer3Holes;
  vector<int>     *mu_nPhiLayer4Holes;
  vector<int>     *mu_nEtaLayer1Holes;
  vector<int>     *mu_nEtaLayer2Holes;
  vector<int>     *mu_nEtaLayer3Holes;
  vector<int>     *mu_nEtaLayer4Holes;
  vector<string>  *trigger_info_chain;
  vector<int>     *trigger_info_isPassed;
  vector<vector<int> > *trigger_info_typeVec;
  vector<vector<float> > *trigger_info_ptVec;
  vector<vector<float> > *trigger_info_etaVec;
  vector<vector<float> > *trigger_info_phiVec;
  vector<float>   *TGC_prd_x;
  vector<float>   *TGC_prd_y;
  vector<float>   *TGC_prd_z;
  vector<float>   *TGC_prd_shortWidth;
  vector<float>   *TGC_prd_longWidth;
  vector<float>   *TGC_prd_length;
  vector<int>     *TGC_prd_isStrip;
  vector<int>     *TGC_prd_gasGap;
  vector<int>     *TGC_prd_channel;
  vector<int>     *TGC_prd_eta;
  vector<int>     *TGC_prd_phi;
  vector<int>     *TGC_prd_station;
  vector<int>     *TGC_prd_bunch;
  Int_t           TGC_coin_n;
  vector<float>   *TGC_coin_x_In;
  vector<float>   *TGC_coin_y_In;
  vector<float>   *TGC_coin_z_In;
  vector<float>   *TGC_coin_x_Out;
  vector<float>   *TGC_coin_y_Out;
  vector<float>   *TGC_coin_z_Out;
  vector<float>   *TGC_coin_width_In;
  vector<float>   *TGC_coin_width_Out;
  vector<float>   *TGC_coin_width_R;
  vector<float>   *TGC_coin_width_Phi;
  vector<int>     *TGC_coin_isAside;
  vector<int>     *TGC_coin_isForward;
  vector<int>     *TGC_coin_isStrip;
  vector<int>     *TGC_coin_isInner;
  vector<int>     *TGC_coin_isPositiveDeltaR;
  vector<int>     *TGC_coin_type;
  vector<int>     *TGC_coin_trackletId;
  vector<int>     *TGC_coin_trackletIdStrip;
  vector<int>     *TGC_coin_phi;
  vector<int>     *TGC_coin_roi;
  vector<int>     *TGC_coin_pt;
  vector<int>     *TGC_coin_delta;
  vector<int>     *TGC_coin_sub;
  vector<int>     *TGC_coin_veto;
  vector<int>     *TGC_coin_bunch;
  vector<int>     *TGC_coin_inner;
  vector<unsigned int> *muctpi_candidateMultiplicities;
  Int_t           muctpi_nDataWords;
  vector<unsigned int> *muctpi_dataWords;
  vector<float>   *muctpi_dw_eta;
  vector<float>   *muctpi_dw_phi;
  vector<short>   *muctpi_dw_source;
  vector<short>   *muctpi_dw_hemisphere;
  vector<short>   *muctpi_dw_bcid;
  vector<short>   *muctpi_dw_sectorID;
  vector<short>   *muctpi_dw_thrNumber;
  vector<short>   *muctpi_dw_roi;
  vector<short>   *muctpi_dw_veto;
  vector<short>   *muctpi_dw_firstCandidate;
  vector<short>   *muctpi_dw_moreCandInRoI;
  vector<short>   *muctpi_dw_moreCandInSector;
  vector<short>   *muctpi_dw_charge;
  vector<short>   *muctpi_dw_candidateVetoed;
  vector<float>   *mc_pt;
  vector<float>   *mc_eta;
  vector<float>   *mc_phi;
  vector<float>   *mc_m;
  vector<int>     *mc_charge;
  vector<int>     *mc_pdgId;
  vector<int>     *mc_barcode;
  vector<int>     *mc_status;
  vector<float>   *mc_prodVtx_x;
  vector<float>   *mc_prodVtx_y;
  vector<float>   *mc_prodVtx_z;
  vector<float>   *rec_mc_pt;
  vector<float>   *rec_mc_eta;
  vector<float>   *rec_mc_phi;
  vector<float>   *rec_mc_e;
  vector<float>   *rec_mc_x;
  vector<float>   *rec_mc_y;
  vector<float>   *rec_mc_z;
  vector<float>   *rec_mc_time;
  vector<int>     *rec_mc_barcode;
  vector<int>     *rec_mc_pdgId;

  // List of branches
  TBranch        *b_runNumber;   //!
  TBranch        *b_EventNumber;   //!
  TBranch        *b_lbn;   //!
  TBranch        *b_bcid;   //!
  TBranch        *b_detmask0;   //!
  TBranch        *b_detmask1;   //!
  TBranch        *b_actualIntPerXing;   //!
  TBranch        *b_averageIntPerXing;   //!
  TBranch        *b_pixelFlags;   //!
  TBranch        *b_sctFlags;   //!
  TBranch        *b_trtFlags;   //!
  TBranch        *b_larFlags;   //!
  TBranch        *b_tileFlags;   //!
  TBranch        *b_muonFlags;   //!
  TBranch        *b_fwdFlags;   //!
  TBranch        *b_coreFlags;   //!
  TBranch        *b_pixelError;   //!
  TBranch        *b_sctError;   //!
  TBranch        *b_trtError;   //!
  TBranch        *b_larError;   //!
  TBranch        *b_tileError;   //!
  TBranch        *b_muonError;   //!
  TBranch        *b_fwdError;   //!
  TBranch        *b_coreError;   //!
  TBranch        *b_trig_L1_mu_n;   //!
  TBranch        *b_trig_L1_mu_eta;   //!
  TBranch        *b_trig_L1_mu_phi;   //!
  TBranch        *b_trig_L1_mu_thrName;   //!
  TBranch        *b_trig_L1_mu_thrNumber;   //!
  TBranch        *b_trig_L1_mu_RoINumber;   //!
  TBranch        *b_trig_L1_mu_sectorAddress;   //!
  TBranch        *b_trig_L1_mu_firstCandidate;   //!
  TBranch        *b_trig_L1_mu_moreCandInRoI;   //!
  TBranch        *b_trig_L1_mu_moreCandInSector;   //!
  TBranch        *b_trig_L1_mu_source;   //!
  TBranch        *b_trig_L1_mu_hemisphere;   //!
  TBranch        *b_trig_L1_mu_charge;   //!
  TBranch        *b_trig_L1_mu_vetoed;   //!
  TBranch        *b_mu_pt;   //!
  TBranch        *b_mu_eta;   //!
  TBranch        *b_mu_phi;   //!
  TBranch        *b_mu_m;   //!
  TBranch        *b_mu_charge;   //!
  TBranch        *b_mu_author;   //!
  TBranch        *b_mu_allAuthors;   //!
  TBranch        *b_mu_muonType;   //!
  TBranch        *b_mu_etcone20;   //!
  TBranch        *b_mu_etcone30;   //!
  TBranch        *b_mu_etcone40;   //!
  TBranch        *b_mu_ptcone20;   //!
  TBranch        *b_mu_ptcone30;   //!
  TBranch        *b_mu_ptcone40;   //!
  TBranch        *b_mu_trackfitchi2;   //!
  TBranch        *b_mu_trackfitndof;   //!
  TBranch        *b_mu_msInnerMatchChi2;   //!
  TBranch        *b_mu_msOuterMatchChi2;   //!
  TBranch        *b_mu_msInnerMatchDOF;   //!
  TBranch        *b_mu_msOuterMatchDOF;   //!
  TBranch        *b_mu_nOutliersOnTrack;   //!
  TBranch        *b_mu_nBLHits;   //!
  TBranch        *b_mu_nPixHits;   //!
  TBranch        *b_mu_nSCTHits;   //!
  TBranch        *b_mu_nTRTHits;   //!
  TBranch        *b_mu_nTRTHighTHits;   //!
  TBranch        *b_mu_nBLSharedHits;   //!
  TBranch        *b_mu_nPixSharedHits;   //!
  TBranch        *b_mu_nPixHoles;   //!
  TBranch        *b_mu_nSCTSharedHits;   //!
  TBranch        *b_mu_nSCTHoles;   //!
  TBranch        *b_mu_nTRTOutliers;   //!
  TBranch        *b_mu_nTRTHighTOutliers;   //!
  TBranch        *b_mu_nGangedPixels;   //!
  TBranch        *b_mu_nPixelDeadSensors;   //!
  TBranch        *b_mu_nSCTDeadSensors;   //!
  TBranch        *b_mu_nTRTDeadStraws;   //!
  TBranch        *b_mu_expectBLayerHit;   //!
  TBranch        *b_mu_nPrecisionLayers;   //!
  TBranch        *b_mu_nPrecisionHoleLayers;   //!
  TBranch        *b_mu_nPhiLayers;   //!
  TBranch        *b_mu_nPhiHoleLayers;   //!
  TBranch        *b_mu_nTrigEtaLayers;   //!
  TBranch        *b_mu_nTrigEtaHoleLayers;   //!
  TBranch        *b_mu_primarySector;   //!
  TBranch        *b_mu_secondarySector;   //!
  TBranch        *b_mu_nInnerSmallHits;   //!
  TBranch        *b_mu_nInnerLargeHits;   //!
  TBranch        *b_mu_nMiddleSmallHits;   //!
  TBranch        *b_mu_nMiddleLargeHits;   //!
  TBranch        *b_mu_nOuterSmallHits;   //!
  TBranch        *b_mu_nOuterLargeHits;   //!
  TBranch        *b_mu_nExtendedSmallHits;   //!
  TBranch        *b_mu_nExtendedLargeHits;   //!
  TBranch        *b_mu_nInnerSmallHoles;   //!
  TBranch        *b_mu_nInnerLargeHoles;   //!
  TBranch        *b_mu_nMiddleSmallHoles;   //!
  TBranch        *b_mu_nMiddleLargeHoles;   //!
  TBranch        *b_mu_nOuterSmallHoles;   //!
  TBranch        *b_mu_nOuterLargeHoles;   //!
  TBranch        *b_mu_nExtendedSmallHoles;   //!
  TBranch        *b_mu_nExtendedLargeHoles;   //!
  TBranch        *b_mu_nPhiLayer1Hits;   //!
  TBranch        *b_mu_nPhiLayer2Hits;   //!
  TBranch        *b_mu_nPhiLayer3Hits;   //!
  TBranch        *b_mu_nPhiLayer4Hits;   //!
  TBranch        *b_mu_nEtaLayer1Hits;   //!
  TBranch        *b_mu_nEtaLayer2Hits;   //!
  TBranch        *b_mu_nEtaLayer3Hits;   //!
  TBranch        *b_mu_nEtaLayer4Hits;   //!
  TBranch        *b_mu_nPhiLayer1Holes;   //!
  TBranch        *b_mu_nPhiLayer2Holes;   //!
  TBranch        *b_mu_nPhiLayer3Holes;   //!
  TBranch        *b_mu_nPhiLayer4Holes;   //!
  TBranch        *b_mu_nEtaLayer1Holes;   //!
  TBranch        *b_mu_nEtaLayer2Holes;   //!
  TBranch        *b_mu_nEtaLayer3Holes;   //!
  TBranch        *b_mu_nEtaLayer4Holes;   //!
  TBranch        *b_trigger_info_chain;   //!
  TBranch        *b_trigger_info_isPassed;   //!
  TBranch        *b_trigger_info_typeVec;   //!
  TBranch        *b_trigger_info_ptVec;   //!
  TBranch        *b_trigger_info_etaVec;   //!
  TBranch        *b_trigger_info_phiVec;   //!
  TBranch        *b_TGC_prd_x;   //!
  TBranch        *b_TGC_prd_y;   //!
  TBranch        *b_TGC_prd_z;   //!
  TBranch        *b_TGC_prd_shortWidth;   //!
  TBranch        *b_TGC_prd_longWidth;   //!
  TBranch        *b_TGC_prd_length;   //!
  TBranch        *b_TGC_prd_isStrip;   //!
  TBranch        *b_TGC_prd_gasGap;   //!
  TBranch        *b_TGC_prd_channel;   //!
  TBranch        *b_TGC_prd_eta;   //!
  TBranch        *b_TGC_prd_phi;   //!
  TBranch        *b_TGC_prd_station;   //!
  TBranch        *b_TGC_prd_bunch;   //!
  TBranch        *b_TGC_coin_n;   //!
  TBranch        *b_TGC_coin_x_In;   //!
  TBranch        *b_TGC_coin_y_In;   //!
  TBranch        *b_TGC_coin_z_In;   //!
  TBranch        *b_TGC_coin_x_Out;   //!
  TBranch        *b_TGC_coin_y_Out;   //!
  TBranch        *b_TGC_coin_z_Out;   //!
  TBranch        *b_TGC_coin_width_In;   //!
  TBranch        *b_TGC_coin_width_Out;   //!
  TBranch        *b_TGC_coin_width_R;   //!
  TBranch        *b_TGC_coin_width_Phi;   //!
  TBranch        *b_TGC_coin_isAside;   //!
  TBranch        *b_TGC_coin_isForward;   //!
  TBranch        *b_TGC_coin_isStrip;   //!
  TBranch        *b_TGC_coin_isInner;   //!
  TBranch        *b_TGC_coin_isPositiveDeltaR;   //!
  TBranch        *b_TGC_coin_type;   //!
  TBranch        *b_TGC_coin_trackletId;   //!
  TBranch        *b_TGC_coin_trackletIdStrip;   //!
  TBranch        *b_TGC_coin_phi;   //!
  TBranch        *b_TGC_coin_roi;   //!
  TBranch        *b_TGC_coin_pt;   //!
  TBranch        *b_TGC_coin_delta;   //!
  TBranch        *b_TGC_coin_sub;   //!
  TBranch        *b_TGC_coin_veto;   //!
  TBranch        *b_TGC_coin_bunch;   //!
  TBranch        *b_TGC_coin_inner;   //!
  TBranch        *b_muctpi_candidateMultiplicities;   //!
  TBranch        *b_muctpi_nDataWords;   //!
  TBranch        *b_muctpi_dataWords;   //!
  TBranch        *b_muctpi_dw_eta;   //!
  TBranch        *b_muctpi_dw_phi;   //!
  TBranch        *b_muctpi_dw_source;   //!
  TBranch        *b_muctpi_dw_hemisphere;   //!
  TBranch        *b_muctpi_dw_bcid;   //!
  TBranch        *b_muctpi_dw_sectorID;   //!
  TBranch        *b_muctpi_dw_thrNumber;   //!
  TBranch        *b_muctpi_dw_roi;   //!
  TBranch        *b_muctpi_dw_veto;   //!
  TBranch        *b_muctpi_dw_firstCandidate;   //!
  TBranch        *b_muctpi_dw_moreCandInRoI;   //!
  TBranch        *b_muctpi_dw_moreCandInSector;   //!
  TBranch        *b_muctpi_dw_charge;   //!
  TBranch        *b_muctpi_dw_candidateVetoed;   //!
  TBranch        *b_mc_pt;   //!
  TBranch        *b_mc_eta;   //!
  TBranch        *b_mc_phi;   //!
  TBranch        *b_mc_m;   //!
  TBranch        *b_mc_charge;   //!
  TBranch        *b_mc_pdgId;   //!
  TBranch        *b_mc_barcode;   //!
  TBranch        *b_mc_status;   //!
  TBranch        *b_mc_prodVtx_x;   //!
  TBranch        *b_mc_prodVtx_y;   //!
  TBranch        *b_mc_prodVtx_z;   //!
  TBranch        *b_rec_mc_pt;   //!
  TBranch        *b_rec_mc_eta;   //!
  TBranch        *b_rec_mc_phi;   //!
  TBranch        *b_rec_mc_e;   //!
  TBranch        *b_rec_mc_x;   //!
  TBranch        *b_rec_mc_y;   //!
  TBranch        *b_rec_mc_z;   //!
  TBranch        *b_rec_mc_time;   //!
  TBranch        *b_rec_mc_barcode;   //!
  TBranch        *b_rec_mc_pdgId;   //!

  TLatex *latex;
  TLatex *latex1;
  TLegend *leg;
  TLegend *leg1;
  TLegend *leg2;
  TLegend *leg3;
  TLegend *leg4;

  TH1D *hbeta[15];
  TH1D *hbetal[15];

  TH1D *hbeta_pass_MU10[15];
  TH1D *hbeta_eff_MU10[15];

  TH1D *hbeta_pass_LATE[15];
  TH1D *hbeta_eff_LATE[15];

  TH1D *hbetaall;

  TH1D *hbeta_pass_MU10all;
  TH1D *hbeta_eff_MU10all;

  TH1D *hbeta_pass_LATEall;
  TH1D *hbeta_eff_LATEall;


  TH1D *hpt[15];

  TH1D *hpt_pass_MU10[15];
  TH1D *hpt_eff_MU10[15];

  TH1D *hpt_pass_LATE[15];
  TH1D *hpt_eff_LATE[15];

  TH1D *hpt_pass[15];
  TH1D *hpt_eff[15];

  TH1D *hptall;
  TH1D *hpt_pass_MU10all;
  TH1D *hpt_eff_MU10all;
  TH1D *hpt_pass_LATEall;
  TH1D *hpt_eff_LATEall;
  TH1D *hpt_pass_LATEall11;
  TH1D *hpt_eff_LATEall1;
  TH1D *hpt_passall;
  TH1D *hpt_effall;

  TH1D *hetaall;
  TH1D *heta_pass_MU10all;
  TH1D *heta_eff_MU10all;
  TH1D *heta_pass_LATEall;
  TH1D *heta_eff_LATEall;
  TH1D *heta_pass_LATEall1;
  TH1D *heta_eff_LATEall1;
  TH1D *heta_passall;
  TH1D *heta_effall;

  TH2D *hpt_eta;
  TH2D *hpt_beta;
  TH2D *heta_beta;
  TH2D *hpt_eta_pass_MU10;
  TH2D *hpt_beta_pass_MU10;
  TH2D *heta_beta_pass_MU10;
  TH2D *hpt_eta_pass_LATE;
  TH2D *hpt_beta_pass_LATE;
  TH2D *heta_beta_pass_LATE;
  TH2D *hpt_eta_pass;
  TH2D *hpt_beta_pass;
  TH2D *hpt_eta_eff_MU10;
  TH2D *hpt_beta_eff_MU10;
  TH2D *heta_beta_eff_MU10;
  TH2D *hpt_eta_eff_LATE;
  TH2D *hpt_beta_eff_LATE;
  TH2D *heta_beta_eff_LATE;
  TH2D *hpt_eta_eff;
  TH2D *hpt_beta_eff;

  //List of TFile
  TFile *file;

  //List of Canvas
  TCanvas *c1;
  TCanvas *c2;
  TCanvas *c3;
  TCanvas *c4;
  TCanvas *c5;
  TCanvas *c6;
  TCanvas *c7;
  TCanvas *c8;
  TCanvas *c9;

  TCanvas *d1;
  TCanvas *d2;
  TCanvas *d3;
  TCanvas *d4;
  TCanvas *d5;
  TCanvas *d6;
  TCanvas *d7;

  L1trigger_matching1(TTree *tree=0);
  virtual ~L1trigger_matching1();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);

  virtual void     SavePDF();
  virtual void     L1InitHistPart1();
  virtual void     L1DrawHistPart1();
  virtual void     L1SaveHist();             
  virtual void     L1SetStyle();

};

#endif

#ifdef L1trigger_matching1_cxx
L1trigger_matching1::L1trigger_matching1(TTree *tree) : fChain(0) 
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  if (tree == 0) {

#ifdef SINGLE_TREE
    // The following code should be used if you want this class to access
    // a single tree instead of a chain
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
    if (!f || !f->IsOpen()) {
      f = new TFile("Memory Directory");
    }
    f->GetObject("physics",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
    TChain * chain = new TChain("physics","physics");
    //chain->Add("/gpfs/fs2001/teramura/sample_0827/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_0827_original_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/sample_0827/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_0827_Allfix_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/sample_0827/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_0827_BWfix_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/sample_0827/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_0827_SWfix_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/sample_0827/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_0827_alltuning_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/addMDT/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_0924_alltuning_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/addMDT/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_0924_original_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/addMDT/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_1018_original1_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/addMDT/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_1018_alltuning_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/addMDT/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_1018_alltuning1_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399106.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_1000_0_3000ns.e7312_e5984_s3421_tuneB_1216_L_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399106.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_1000_0_3000ns.e7312_e5984_s3421_tuneE_1216_L_LightTGCNtuple/*.root/physics");

    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399106.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_1000_0_3000ns.e7312_e5984_tuneA_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399106.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_1000_0_3000ns.e7312_e5984_tuneB_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399106.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_1000_0_3000ns.e7312_e5984_tuneC_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399106.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_1000_0_3000ns.e7312_e5984_tuneD_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399106.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_1000_0_3000ns.e7312_e5984_tuneE_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneA_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneB_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneC_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneD_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneE_1216_LLL_LightTGCNtuple/*.root/physics");

    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_oriA_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_oriB_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_oriC_1216_LLL_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneA_1216_T_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneB_1216_T_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneC_1216_T_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneD_1216_T_LightTGCNtuple/*.root/physics");
    //chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_tuneE_1216_T_LightTGCNtuple/*.root/physics");
    chain->Add("/gpfs/fs2001/teramura/master/user.teramura.mc16_13TeV.399079.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_600_0_3000ns.e7312_s3421_oriA_1216_T_LightTGCNtuple/*.root/physics");
    tree = chain;
#endif // SINGLE_TREE

  }
  Init(tree);
}

L1trigger_matching1::~L1trigger_matching1()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t L1trigger_matching1::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t L1trigger_matching1::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void L1trigger_matching1::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  trig_L1_mu_eta = 0;
  trig_L1_mu_phi = 0;
  trig_L1_mu_thrName = 0;
  trig_L1_mu_thrNumber = 0;
  trig_L1_mu_RoINumber = 0;
  trig_L1_mu_sectorAddress = 0;
  trig_L1_mu_firstCandidate = 0;
  trig_L1_mu_moreCandInRoI = 0;
  trig_L1_mu_moreCandInSector = 0;
  trig_L1_mu_source = 0;
  trig_L1_mu_hemisphere = 0;
  trig_L1_mu_charge = 0;
  trig_L1_mu_vetoed = 0;
  mu_pt = 0;
  mu_eta = 0;
  mu_phi = 0;
  mu_m = 0;
  mu_charge = 0;
  mu_author = 0;
  mu_allAuthors = 0;
  mu_muonType = 0;
  mu_etcone20 = 0;
  mu_etcone30 = 0;
  mu_etcone40 = 0;
  mu_ptcone20 = 0;
  mu_ptcone30 = 0;
  mu_ptcone40 = 0;
  mu_trackfitchi2 = 0;
  mu_trackfitndof = 0;
  mu_msInnerMatchChi2 = 0;
  mu_msOuterMatchChi2 = 0;
  mu_msInnerMatchDOF = 0;
  mu_msOuterMatchDOF = 0;
  mu_nOutliersOnTrack = 0;
  mu_nBLHits = 0;
  mu_nPixHits = 0;
  mu_nSCTHits = 0;
  mu_nTRTHits = 0;
  mu_nTRTHighTHits = 0;
  mu_nBLSharedHits = 0;
  mu_nPixSharedHits = 0;
  mu_nPixHoles = 0;
  mu_nSCTSharedHits = 0;
  mu_nSCTHoles = 0;
  mu_nTRTOutliers = 0;
  mu_nTRTHighTOutliers = 0;
  mu_nGangedPixels = 0;
  mu_nPixelDeadSensors = 0;
  mu_nSCTDeadSensors = 0;
  mu_nTRTDeadStraws = 0;
  mu_expectBLayerHit = 0;
  mu_nPrecisionLayers = 0;
  mu_nPrecisionHoleLayers = 0;
  mu_nPhiLayers = 0;
  mu_nPhiHoleLayers = 0;
  mu_nTrigEtaLayers = 0;
  mu_nTrigEtaHoleLayers = 0;
  mu_primarySector = 0;
  mu_secondarySector = 0;
  mu_nInnerSmallHits = 0;
  mu_nInnerLargeHits = 0;
  mu_nMiddleSmallHits = 0;
  mu_nMiddleLargeHits = 0;
  mu_nOuterSmallHits = 0;
  mu_nOuterLargeHits = 0;
  mu_nExtendedSmallHits = 0;
  mu_nExtendedLargeHits = 0;
  mu_nInnerSmallHoles = 0;
  mu_nInnerLargeHoles = 0;
  mu_nMiddleSmallHoles = 0;
  mu_nMiddleLargeHoles = 0;
  mu_nOuterSmallHoles = 0;
  mu_nOuterLargeHoles = 0;
  mu_nExtendedSmallHoles = 0;
  mu_nExtendedLargeHoles = 0;
  mu_nPhiLayer1Hits = 0;
  mu_nPhiLayer2Hits = 0;
  mu_nPhiLayer3Hits = 0;
  mu_nPhiLayer4Hits = 0;
  mu_nEtaLayer1Hits = 0;
  mu_nEtaLayer2Hits = 0;
  mu_nEtaLayer3Hits = 0;
  mu_nEtaLayer4Hits = 0;
  mu_nPhiLayer1Holes = 0;
  mu_nPhiLayer2Holes = 0;
  mu_nPhiLayer3Holes = 0;
  mu_nPhiLayer4Holes = 0;
  mu_nEtaLayer1Holes = 0;
  mu_nEtaLayer2Holes = 0;
  mu_nEtaLayer3Holes = 0;
  mu_nEtaLayer4Holes = 0;
  trigger_info_chain = 0;
  trigger_info_isPassed = 0;
  trigger_info_typeVec = 0;
  trigger_info_ptVec = 0;
  trigger_info_etaVec = 0;
  trigger_info_phiVec = 0;
  TGC_prd_x = 0;
  TGC_prd_y = 0;
  TGC_prd_z = 0;
  TGC_prd_shortWidth = 0;
  TGC_prd_longWidth = 0;
  TGC_prd_length = 0;
  TGC_prd_isStrip = 0;
  TGC_prd_gasGap = 0;
  TGC_prd_channel = 0;
  TGC_prd_eta = 0;
  TGC_prd_phi = 0;
  TGC_prd_station = 0;
  TGC_prd_bunch = 0;
  TGC_coin_x_In = 0;
  TGC_coin_y_In = 0;
  TGC_coin_z_In = 0;
  TGC_coin_x_Out = 0;
  TGC_coin_y_Out = 0;
  TGC_coin_z_Out = 0;
  TGC_coin_width_In = 0;
  TGC_coin_width_Out = 0;
  TGC_coin_width_R = 0;
  TGC_coin_width_Phi = 0;
  TGC_coin_isAside = 0;
  TGC_coin_isForward = 0;
  TGC_coin_isStrip = 0;
  TGC_coin_isInner = 0;
  TGC_coin_isPositiveDeltaR = 0;
  TGC_coin_type = 0;
  TGC_coin_trackletId = 0;
  TGC_coin_trackletIdStrip = 0;
  TGC_coin_phi = 0;
  TGC_coin_roi = 0;
  TGC_coin_pt = 0;
  TGC_coin_delta = 0;
  TGC_coin_sub = 0;
  TGC_coin_veto = 0;
  TGC_coin_bunch = 0;
  TGC_coin_inner = 0;
  muctpi_candidateMultiplicities = 0;
  muctpi_dataWords = 0;
  muctpi_dw_eta = 0;
  muctpi_dw_phi = 0;
  muctpi_dw_source = 0;
  muctpi_dw_hemisphere = 0;
  muctpi_dw_bcid = 0;
  muctpi_dw_sectorID = 0;
  muctpi_dw_thrNumber = 0;
  muctpi_dw_roi = 0;
  muctpi_dw_veto = 0;
  muctpi_dw_firstCandidate = 0;
  muctpi_dw_moreCandInRoI = 0;
  muctpi_dw_moreCandInSector = 0;
  muctpi_dw_charge = 0;
  muctpi_dw_candidateVetoed = 0;
  mc_pt = 0;
  mc_eta = 0;
  mc_phi = 0;
  mc_m = 0;
  mc_charge = 0;
  mc_pdgId = 0;
  mc_barcode = 0;
  mc_status = 0;
  mc_prodVtx_x = 0;
  mc_prodVtx_y = 0;
  mc_prodVtx_z = 0;
  rec_mc_pt = 0;
  rec_mc_eta = 0;
  rec_mc_phi = 0;
  rec_mc_e = 0;
  rec_mc_x = 0;
  rec_mc_y = 0;
  rec_mc_z = 0;
  rec_mc_time = 0;
  rec_mc_barcode = 0;
  rec_mc_pdgId = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("RunNumber", &RunNumber, &b_runNumber);
  fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
  fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
  fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
  fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
  fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
  fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
  fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
  fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
  fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
  fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
  fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
  fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
  fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
  fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
  fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
  fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
  fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
  fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
  fChain->SetBranchAddress("larError", &larError, &b_larError);
  fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
  fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
  fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
  fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
  fChain->SetBranchAddress("trig_L1_mu_eta", &trig_L1_mu_eta, &b_trig_L1_mu_eta);
  fChain->SetBranchAddress("trig_L1_mu_phi", &trig_L1_mu_phi, &b_trig_L1_mu_phi);
  fChain->SetBranchAddress("trig_L1_mu_thrName", &trig_L1_mu_thrName, &b_trig_L1_mu_thrName);
  fChain->SetBranchAddress("trig_L1_mu_thrNumber", &trig_L1_mu_thrNumber, &b_trig_L1_mu_thrNumber);
  fChain->SetBranchAddress("trig_L1_mu_RoINumber", &trig_L1_mu_RoINumber, &b_trig_L1_mu_RoINumber);
  fChain->SetBranchAddress("trig_L1_mu_sectorAddress", &trig_L1_mu_sectorAddress, &b_trig_L1_mu_sectorAddress);
  fChain->SetBranchAddress("trig_L1_mu_firstCandidate", &trig_L1_mu_firstCandidate, &b_trig_L1_mu_firstCandidate);
  fChain->SetBranchAddress("trig_L1_mu_moreCandInRoI", &trig_L1_mu_moreCandInRoI, &b_trig_L1_mu_moreCandInRoI);
  fChain->SetBranchAddress("trig_L1_mu_moreCandInSector", &trig_L1_mu_moreCandInSector, &b_trig_L1_mu_moreCandInSector);
  fChain->SetBranchAddress("trig_L1_mu_source", &trig_L1_mu_source, &b_trig_L1_mu_source);
  fChain->SetBranchAddress("trig_L1_mu_hemisphere", &trig_L1_mu_hemisphere, &b_trig_L1_mu_hemisphere);
  fChain->SetBranchAddress("trig_L1_mu_charge", &trig_L1_mu_charge, &b_trig_L1_mu_charge);
  fChain->SetBranchAddress("trig_L1_mu_vetoed", &trig_L1_mu_vetoed, &b_trig_L1_mu_vetoed);
  fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
  fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
  fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
  fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
  fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
  fChain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
  fChain->SetBranchAddress("mu_allAuthors", &mu_allAuthors, &b_mu_allAuthors);
  fChain->SetBranchAddress("mu_muonType", &mu_muonType, &b_mu_muonType);
  fChain->SetBranchAddress("mu_etcone20", &mu_etcone20, &b_mu_etcone20);
  fChain->SetBranchAddress("mu_etcone30", &mu_etcone30, &b_mu_etcone30);
  fChain->SetBranchAddress("mu_etcone40", &mu_etcone40, &b_mu_etcone40);
  fChain->SetBranchAddress("mu_ptcone20", &mu_ptcone20, &b_mu_ptcone20);
  fChain->SetBranchAddress("mu_ptcone30", &mu_ptcone30, &b_mu_ptcone30);
  fChain->SetBranchAddress("mu_ptcone40", &mu_ptcone40, &b_mu_ptcone40);
  fChain->SetBranchAddress("mu_trackfitchi2", &mu_trackfitchi2, &b_mu_trackfitchi2);
  fChain->SetBranchAddress("mu_trackfitndof", &mu_trackfitndof, &b_mu_trackfitndof);
  fChain->SetBranchAddress("mu_msInnerMatchChi2", &mu_msInnerMatchChi2, &b_mu_msInnerMatchChi2);
  fChain->SetBranchAddress("mu_msOuterMatchChi2", &mu_msOuterMatchChi2, &b_mu_msOuterMatchChi2);
  fChain->SetBranchAddress("mu_msInnerMatchDOF", &mu_msInnerMatchDOF, &b_mu_msInnerMatchDOF);
  fChain->SetBranchAddress("mu_msOuterMatchDOF", &mu_msOuterMatchDOF, &b_mu_msOuterMatchDOF);
  fChain->SetBranchAddress("mu_nOutliersOnTrack", &mu_nOutliersOnTrack, &b_mu_nOutliersOnTrack);
  fChain->SetBranchAddress("mu_nBLHits", &mu_nBLHits, &b_mu_nBLHits);
  fChain->SetBranchAddress("mu_nPixHits", &mu_nPixHits, &b_mu_nPixHits);
  fChain->SetBranchAddress("mu_nSCTHits", &mu_nSCTHits, &b_mu_nSCTHits);
  fChain->SetBranchAddress("mu_nTRTHits", &mu_nTRTHits, &b_mu_nTRTHits);
  fChain->SetBranchAddress("mu_nTRTHighTHits", &mu_nTRTHighTHits, &b_mu_nTRTHighTHits);
  fChain->SetBranchAddress("mu_nBLSharedHits", &mu_nBLSharedHits, &b_mu_nBLSharedHits);
  fChain->SetBranchAddress("mu_nPixSharedHits", &mu_nPixSharedHits, &b_mu_nPixSharedHits);
  fChain->SetBranchAddress("mu_nPixHoles", &mu_nPixHoles, &b_mu_nPixHoles);
  fChain->SetBranchAddress("mu_nSCTSharedHits", &mu_nSCTSharedHits, &b_mu_nSCTSharedHits);
  fChain->SetBranchAddress("mu_nSCTHoles", &mu_nSCTHoles, &b_mu_nSCTHoles);
  fChain->SetBranchAddress("mu_nTRTOutliers", &mu_nTRTOutliers, &b_mu_nTRTOutliers);
  fChain->SetBranchAddress("mu_nTRTHighTOutliers", &mu_nTRTHighTOutliers, &b_mu_nTRTHighTOutliers);
  fChain->SetBranchAddress("mu_nGangedPixels", &mu_nGangedPixels, &b_mu_nGangedPixels);
  fChain->SetBranchAddress("mu_nPixelDeadSensors", &mu_nPixelDeadSensors, &b_mu_nPixelDeadSensors);
  fChain->SetBranchAddress("mu_nSCTDeadSensors", &mu_nSCTDeadSensors, &b_mu_nSCTDeadSensors);
  fChain->SetBranchAddress("mu_nTRTDeadStraws", &mu_nTRTDeadStraws, &b_mu_nTRTDeadStraws);
  fChain->SetBranchAddress("mu_expectBLayerHit", &mu_expectBLayerHit, &b_mu_expectBLayerHit);
  fChain->SetBranchAddress("mu_nPrecisionLayers", &mu_nPrecisionLayers, &b_mu_nPrecisionLayers);
  fChain->SetBranchAddress("mu_nPrecisionHoleLayers", &mu_nPrecisionHoleLayers, &b_mu_nPrecisionHoleLayers);
  fChain->SetBranchAddress("mu_nPhiLayers", &mu_nPhiLayers, &b_mu_nPhiLayers);
  fChain->SetBranchAddress("mu_nPhiHoleLayers", &mu_nPhiHoleLayers, &b_mu_nPhiHoleLayers);
  fChain->SetBranchAddress("mu_nTrigEtaLayers", &mu_nTrigEtaLayers, &b_mu_nTrigEtaLayers);
  fChain->SetBranchAddress("mu_nTrigEtaHoleLayers", &mu_nTrigEtaHoleLayers, &b_mu_nTrigEtaHoleLayers);
  fChain->SetBranchAddress("mu_primarySector", &mu_primarySector, &b_mu_primarySector);
  fChain->SetBranchAddress("mu_secondarySector", &mu_secondarySector, &b_mu_secondarySector);
  fChain->SetBranchAddress("mu_nInnerSmallHits", &mu_nInnerSmallHits, &b_mu_nInnerSmallHits);
  fChain->SetBranchAddress("mu_nInnerLargeHits", &mu_nInnerLargeHits, &b_mu_nInnerLargeHits);
  fChain->SetBranchAddress("mu_nMiddleSmallHits", &mu_nMiddleSmallHits, &b_mu_nMiddleSmallHits);
  fChain->SetBranchAddress("mu_nMiddleLargeHits", &mu_nMiddleLargeHits, &b_mu_nMiddleLargeHits);
  fChain->SetBranchAddress("mu_nOuterSmallHits", &mu_nOuterSmallHits, &b_mu_nOuterSmallHits);
  fChain->SetBranchAddress("mu_nOuterLargeHits", &mu_nOuterLargeHits, &b_mu_nOuterLargeHits);
  fChain->SetBranchAddress("mu_nExtendedSmallHits", &mu_nExtendedSmallHits, &b_mu_nExtendedSmallHits);
  fChain->SetBranchAddress("mu_nExtendedLargeHits", &mu_nExtendedLargeHits, &b_mu_nExtendedLargeHits);
  fChain->SetBranchAddress("mu_nInnerSmallHoles", &mu_nInnerSmallHoles, &b_mu_nInnerSmallHoles);
  fChain->SetBranchAddress("mu_nInnerLargeHoles", &mu_nInnerLargeHoles, &b_mu_nInnerLargeHoles);
  fChain->SetBranchAddress("mu_nMiddleSmallHoles", &mu_nMiddleSmallHoles, &b_mu_nMiddleSmallHoles);
  fChain->SetBranchAddress("mu_nMiddleLargeHoles", &mu_nMiddleLargeHoles, &b_mu_nMiddleLargeHoles);
  fChain->SetBranchAddress("mu_nOuterSmallHoles", &mu_nOuterSmallHoles, &b_mu_nOuterSmallHoles);
  fChain->SetBranchAddress("mu_nOuterLargeHoles", &mu_nOuterLargeHoles, &b_mu_nOuterLargeHoles);
  fChain->SetBranchAddress("mu_nExtendedSmallHoles", &mu_nExtendedSmallHoles, &b_mu_nExtendedSmallHoles);
  fChain->SetBranchAddress("mu_nExtendedLargeHoles", &mu_nExtendedLargeHoles, &b_mu_nExtendedLargeHoles);
  fChain->SetBranchAddress("mu_nPhiLayer1Hits", &mu_nPhiLayer1Hits, &b_mu_nPhiLayer1Hits);
  fChain->SetBranchAddress("mu_nPhiLayer2Hits", &mu_nPhiLayer2Hits, &b_mu_nPhiLayer2Hits);
  fChain->SetBranchAddress("mu_nPhiLayer3Hits", &mu_nPhiLayer3Hits, &b_mu_nPhiLayer3Hits);
  fChain->SetBranchAddress("mu_nPhiLayer4Hits", &mu_nPhiLayer4Hits, &b_mu_nPhiLayer4Hits);
  fChain->SetBranchAddress("mu_nEtaLayer1Hits", &mu_nEtaLayer1Hits, &b_mu_nEtaLayer1Hits);
  fChain->SetBranchAddress("mu_nEtaLayer2Hits", &mu_nEtaLayer2Hits, &b_mu_nEtaLayer2Hits);
  fChain->SetBranchAddress("mu_nEtaLayer3Hits", &mu_nEtaLayer3Hits, &b_mu_nEtaLayer3Hits);
  fChain->SetBranchAddress("mu_nEtaLayer4Hits", &mu_nEtaLayer4Hits, &b_mu_nEtaLayer4Hits);
  fChain->SetBranchAddress("mu_nPhiLayer1Holes", &mu_nPhiLayer1Holes, &b_mu_nPhiLayer1Holes);
  fChain->SetBranchAddress("mu_nPhiLayer2Holes", &mu_nPhiLayer2Holes, &b_mu_nPhiLayer2Holes);
  fChain->SetBranchAddress("mu_nPhiLayer3Holes", &mu_nPhiLayer3Holes, &b_mu_nPhiLayer3Holes);
  fChain->SetBranchAddress("mu_nPhiLayer4Holes", &mu_nPhiLayer4Holes, &b_mu_nPhiLayer4Holes);
  fChain->SetBranchAddress("mu_nEtaLayer1Holes", &mu_nEtaLayer1Holes, &b_mu_nEtaLayer1Holes);
  fChain->SetBranchAddress("mu_nEtaLayer2Holes", &mu_nEtaLayer2Holes, &b_mu_nEtaLayer2Holes);
  fChain->SetBranchAddress("mu_nEtaLayer3Holes", &mu_nEtaLayer3Holes, &b_mu_nEtaLayer3Holes);
  fChain->SetBranchAddress("mu_nEtaLayer4Holes", &mu_nEtaLayer4Holes, &b_mu_nEtaLayer4Holes);
  fChain->SetBranchAddress("trigger_info_chain", &trigger_info_chain, &b_trigger_info_chain);
  fChain->SetBranchAddress("trigger_info_isPassed", &trigger_info_isPassed, &b_trigger_info_isPassed);
  fChain->SetBranchAddress("trigger_info_typeVec", &trigger_info_typeVec, &b_trigger_info_typeVec);
  fChain->SetBranchAddress("trigger_info_ptVec", &trigger_info_ptVec, &b_trigger_info_ptVec);
  fChain->SetBranchAddress("trigger_info_etaVec", &trigger_info_etaVec, &b_trigger_info_etaVec);
  fChain->SetBranchAddress("trigger_info_phiVec", &trigger_info_phiVec, &b_trigger_info_phiVec);
  fChain->SetBranchAddress("TGC_prd_x", &TGC_prd_x, &b_TGC_prd_x);
  fChain->SetBranchAddress("TGC_prd_y", &TGC_prd_y, &b_TGC_prd_y);
  fChain->SetBranchAddress("TGC_prd_z", &TGC_prd_z, &b_TGC_prd_z);
  fChain->SetBranchAddress("TGC_prd_shortWidth", &TGC_prd_shortWidth, &b_TGC_prd_shortWidth);
  fChain->SetBranchAddress("TGC_prd_longWidth", &TGC_prd_longWidth, &b_TGC_prd_longWidth);
  fChain->SetBranchAddress("TGC_prd_length", &TGC_prd_length, &b_TGC_prd_length);
  fChain->SetBranchAddress("TGC_prd_isStrip", &TGC_prd_isStrip, &b_TGC_prd_isStrip);
  fChain->SetBranchAddress("TGC_prd_gasGap", &TGC_prd_gasGap, &b_TGC_prd_gasGap);
  fChain->SetBranchAddress("TGC_prd_channel", &TGC_prd_channel, &b_TGC_prd_channel);
  fChain->SetBranchAddress("TGC_prd_eta", &TGC_prd_eta, &b_TGC_prd_eta);
  fChain->SetBranchAddress("TGC_prd_phi", &TGC_prd_phi, &b_TGC_prd_phi);
  fChain->SetBranchAddress("TGC_prd_station", &TGC_prd_station, &b_TGC_prd_station);
  fChain->SetBranchAddress("TGC_prd_bunch", &TGC_prd_bunch, &b_TGC_prd_bunch);
  fChain->SetBranchAddress("TGC_coin_n", &TGC_coin_n, &b_TGC_coin_n);
  fChain->SetBranchAddress("TGC_coin_x_In", &TGC_coin_x_In, &b_TGC_coin_x_In);
  fChain->SetBranchAddress("TGC_coin_y_In", &TGC_coin_y_In, &b_TGC_coin_y_In);
  fChain->SetBranchAddress("TGC_coin_z_In", &TGC_coin_z_In, &b_TGC_coin_z_In);
  fChain->SetBranchAddress("TGC_coin_x_Out", &TGC_coin_x_Out, &b_TGC_coin_x_Out);
  fChain->SetBranchAddress("TGC_coin_y_Out", &TGC_coin_y_Out, &b_TGC_coin_y_Out);
  fChain->SetBranchAddress("TGC_coin_z_Out", &TGC_coin_z_Out, &b_TGC_coin_z_Out);
  fChain->SetBranchAddress("TGC_coin_width_In", &TGC_coin_width_In, &b_TGC_coin_width_In);
  fChain->SetBranchAddress("TGC_coin_width_Out", &TGC_coin_width_Out, &b_TGC_coin_width_Out);
  fChain->SetBranchAddress("TGC_coin_width_R", &TGC_coin_width_R, &b_TGC_coin_width_R);
  fChain->SetBranchAddress("TGC_coin_width_Phi", &TGC_coin_width_Phi, &b_TGC_coin_width_Phi);
  fChain->SetBranchAddress("TGC_coin_isAside", &TGC_coin_isAside, &b_TGC_coin_isAside);
  fChain->SetBranchAddress("TGC_coin_isForward", &TGC_coin_isForward, &b_TGC_coin_isForward);
  fChain->SetBranchAddress("TGC_coin_isStrip", &TGC_coin_isStrip, &b_TGC_coin_isStrip);
  fChain->SetBranchAddress("TGC_coin_isInner", &TGC_coin_isInner, &b_TGC_coin_isInner);
  fChain->SetBranchAddress("TGC_coin_isPositiveDeltaR", &TGC_coin_isPositiveDeltaR, &b_TGC_coin_isPositiveDeltaR);
  fChain->SetBranchAddress("TGC_coin_type", &TGC_coin_type, &b_TGC_coin_type);
  fChain->SetBranchAddress("TGC_coin_trackletId", &TGC_coin_trackletId, &b_TGC_coin_trackletId);
  fChain->SetBranchAddress("TGC_coin_trackletIdStrip", &TGC_coin_trackletIdStrip, &b_TGC_coin_trackletIdStrip);
  fChain->SetBranchAddress("TGC_coin_phi", &TGC_coin_phi, &b_TGC_coin_phi);
  fChain->SetBranchAddress("TGC_coin_roi", &TGC_coin_roi, &b_TGC_coin_roi);
  fChain->SetBranchAddress("TGC_coin_pt", &TGC_coin_pt, &b_TGC_coin_pt);
  fChain->SetBranchAddress("TGC_coin_delta", &TGC_coin_delta, &b_TGC_coin_delta);
  fChain->SetBranchAddress("TGC_coin_sub", &TGC_coin_sub, &b_TGC_coin_sub);
  fChain->SetBranchAddress("TGC_coin_veto", &TGC_coin_veto, &b_TGC_coin_veto);
  fChain->SetBranchAddress("TGC_coin_bunch", &TGC_coin_bunch, &b_TGC_coin_bunch);
  fChain->SetBranchAddress("TGC_coin_inner", &TGC_coin_inner, &b_TGC_coin_inner);
  fChain->SetBranchAddress("muctpi_candidateMultiplicities", &muctpi_candidateMultiplicities, &b_muctpi_candidateMultiplicities);
  fChain->SetBranchAddress("muctpi_nDataWords", &muctpi_nDataWords, &b_muctpi_nDataWords);
  fChain->SetBranchAddress("muctpi_dataWords", &muctpi_dataWords, &b_muctpi_dataWords);
  fChain->SetBranchAddress("muctpi_dw_eta", &muctpi_dw_eta, &b_muctpi_dw_eta);
  fChain->SetBranchAddress("muctpi_dw_phi", &muctpi_dw_phi, &b_muctpi_dw_phi);
  fChain->SetBranchAddress("muctpi_dw_source", &muctpi_dw_source, &b_muctpi_dw_source);
  fChain->SetBranchAddress("muctpi_dw_hemisphere", &muctpi_dw_hemisphere, &b_muctpi_dw_hemisphere);
  fChain->SetBranchAddress("muctpi_dw_bcid", &muctpi_dw_bcid, &b_muctpi_dw_bcid);
  fChain->SetBranchAddress("muctpi_dw_sectorID", &muctpi_dw_sectorID, &b_muctpi_dw_sectorID);
  fChain->SetBranchAddress("muctpi_dw_thrNumber", &muctpi_dw_thrNumber, &b_muctpi_dw_thrNumber);
  fChain->SetBranchAddress("muctpi_dw_roi", &muctpi_dw_roi, &b_muctpi_dw_roi);
  fChain->SetBranchAddress("muctpi_dw_veto", &muctpi_dw_veto, &b_muctpi_dw_veto);
  fChain->SetBranchAddress("muctpi_dw_firstCandidate", &muctpi_dw_firstCandidate, &b_muctpi_dw_firstCandidate);
  fChain->SetBranchAddress("muctpi_dw_moreCandInRoI", &muctpi_dw_moreCandInRoI, &b_muctpi_dw_moreCandInRoI);
  fChain->SetBranchAddress("muctpi_dw_moreCandInSector", &muctpi_dw_moreCandInSector, &b_muctpi_dw_moreCandInSector);
  fChain->SetBranchAddress("muctpi_dw_charge", &muctpi_dw_charge, &b_muctpi_dw_charge);
  fChain->SetBranchAddress("muctpi_dw_candidateVetoed", &muctpi_dw_candidateVetoed, &b_muctpi_dw_candidateVetoed);
  fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
  fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
  fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
  fChain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
  fChain->SetBranchAddress("mc_charge", &mc_charge, &b_mc_charge);
  fChain->SetBranchAddress("mc_pdgId", &mc_pdgId, &b_mc_pdgId);
  fChain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
  fChain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
  fChain->SetBranchAddress("mc_prodVtx_x", &mc_prodVtx_x, &b_mc_prodVtx_x);
  fChain->SetBranchAddress("mc_prodVtx_y", &mc_prodVtx_y, &b_mc_prodVtx_y);
  fChain->SetBranchAddress("mc_prodVtx_z", &mc_prodVtx_z, &b_mc_prodVtx_z);
  fChain->SetBranchAddress("rec_mc_pt", &rec_mc_pt, &b_rec_mc_pt);
  fChain->SetBranchAddress("rec_mc_eta", &rec_mc_eta, &b_rec_mc_eta);
  fChain->SetBranchAddress("rec_mc_phi", &rec_mc_phi, &b_rec_mc_phi);
  fChain->SetBranchAddress("rec_mc_e", &rec_mc_e, &b_rec_mc_e);
  fChain->SetBranchAddress("rec_mc_x", &rec_mc_x, &b_rec_mc_x);
  fChain->SetBranchAddress("rec_mc_y", &rec_mc_y, &b_rec_mc_y);
  fChain->SetBranchAddress("rec_mc_z", &rec_mc_z, &b_rec_mc_z);
  fChain->SetBranchAddress("rec_mc_time", &rec_mc_time, &b_rec_mc_time);
  fChain->SetBranchAddress("rec_mc_barcode", &rec_mc_barcode, &b_rec_mc_barcode);
  fChain->SetBranchAddress("rec_mc_pdgId", &rec_mc_pdgId, &b_rec_mc_pdgId);
  Notify();

  L1SetStyle();
  L1InitHistPart1();
}

Bool_t L1trigger_matching1::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void L1trigger_matching1::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t L1trigger_matching1::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}
#endif // #ifdef L1trigger_matching1_cxx
