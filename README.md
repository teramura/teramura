# BXstudy (chap.5)
Obtain BCID ratios for each chamber.
# HitEfficiency (chap.5)
Calculate the hit efficiency of the TGC.
# TgcTimingCalibration (chap.5)
Calibration of each chamber using timing parameters and create the Ntuple for timing calibration.
# EstimateEfficiency (chap.6)
Estimate trigger efficiency from BCID ratios.
# PDF (chap.6)
Get the probability distribution function from BCID ratios.
# TriggerEfficiency (chap.4, chap.6)
Calculate the trigger efficiency in the single muon trigger and teh late muon trigger.
```
$ make clean
$ make
$ ./L1
```
# LightTGCNtuple
Make Ntuple for TGC study. 
```
$ cd LightTGCNtuple
$ mkdir run build
$ cd build
$ asetup Athena 22.0.14
$ cmake ../athena/Projects/Workdir
$ make 
$ source x86_64-centos7-gcc62-opt/setup.sh
$ cd ../run
$ cp ../athena/LightTGCNtuple/share/runLightTGCNtuple.py .
put ESD root file
$ athena.py runLightTGCNtuple.py
```
