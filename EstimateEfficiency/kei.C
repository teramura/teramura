#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void kei()
{
  gStyle->SetLabelSize(0.06,"y");
  gStyle->SetTitleSize(0.06,"x");
  gStyle->SetTitleSize(0.06,"y");
  gStyle->SetLabelSize(0.06,"x");

  TFile *file;
  file = new TFile("tune_blue_1.root","recreate");

  
  float length1 = 28.6;
  float length0 = 4.32;
  float length11 = 32.5;
  float length00 = 8.4;
  float wv = 14.66;
  float sv = 20.44;  //mctune
  
  /*
  float length1 = 25.37;
  float length0 = 1.3;
  float length11 = 30.83;
  float length00 = 1.4;
  float wv = 14.66;
  float sv = 20.44;  //mcori
  */
  /*  
  float length1 = 27.8;
  float length0 = 0.;
  float length11 = 35.1;
  float length00 = 0.;
  float wv = 14.66;
  float sv = 20.44;  //data
  */
  const unsigned int etanum = 15;
  int betanum = 20;
  float Wwindow = 29.32;
  float Swindow = 40.94;
  float BX      = 25.00;

  float xW[2] = {wv, length1};
  float XW[2] = {length0 , wv};
  float xS[2] = {sv, length11};
  float XS[2] = {length00 , sv};
  float y[2] = {1, 0};
  float Y[2] = {0, 1};
  float lengthW = xW[1] - XW[0];
  float lengthS = xS[1] - XS[0];


  float eta[etanum] = {1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4};
  float bM1[etanum][betanum]; //eta beta
  float bM3[etanum][betanum]; //eta beta

  float xM1W[etanum][betanum][2];
  float xM3W[etanum][betanum][2];
  float XM1W[etanum][betanum][2];
  float XM3W[etanum][betanum][2];
  float xM1S[etanum][betanum][2];
  float xM3S[etanum][betanum][2];
  float XM1S[etanum][betanum][2];
  float XM3S[etanum][betanum][2];

  TGraph *gM1W[etanum][betanum];
  TGraph *gM3W[etanum][betanum];
  TGraph *GM1W[etanum][betanum];
  TGraph *GM3W[etanum][betanum];
  TGraph *gM1S[etanum][betanum];
  TGraph *gM3S[etanum][betanum];
  TGraph *GM1S[etanum][betanum];
  TGraph *GM3S[etanum][betanum];

  TF1 *fM1W[etanum][betanum];
  TF1 *fM3W[etanum][betanum];
  TF1 *FM1W[etanum][betanum];
  TF1 *FM3W[etanum][betanum];
  TF1 *fM1S[etanum][betanum];
  TF1 *fM3S[etanum][betanum];
  TF1 *FM1S[etanum][betanum];
  TF1 *FM3S[etanum][betanum];

  float currM1W[etanum][betanum];
  float currM3W[etanum][betanum];
  float nextM1W[etanum][betanum];
  float nextM3W[etanum][betanum];
  float currM1S[etanum][betanum];
  float currM3S[etanum][betanum];
  float nextM1S[etanum][betanum];
  float nextM3S[etanum][betanum];

  float ex[betanum];

  for(int b = 0;b < betanum;b++) {
    ex[b] = b * 1./betanum + 1./betanum;
    for(int e = 0;e < etanum;e++) {
      TVector3 v;
      v.SetPtEtaPhi(99,eta[e],99);
      float z[5] = {7030, 7425, 13605, 14860, 15280}; //FI EI M1 M2 M3
      float L[5];
      for(int i = 0;i < 5;i++) {
	L[i] = z[i] / TMath::Cos(v.Theta());
	if(i == 2) bM1[e][b] = L[i] / 3.0 * pow(10, -2) / (b * 1./betanum + 1./betanum) - L[i] / 3.0 * pow(10, -2);
	if(i == 4) bM3[e][b] = L[i] / 3.0 * pow(10, -2) / (b * 1./betanum + 1./betanum) - L[i] / 3.0 * pow(10, -2);
      }
    }
  }

  for(int e = 0;e < etanum;e++) {
    for(int b = 0;b < betanum;b++) {
      for(int i = 0;i < 2;i++) {
	xM1W[e][b][i] = xW[i] + bM1[e][b];
	xM3W[e][b][i] = xW[i] + bM3[e][b];
	XM1W[e][b][i] = XW[i] + bM1[e][b];
	XM3W[e][b][i] = XW[i] + bM3[e][b];
	xM1S[e][b][i] = xS[i] + bM1[e][b];
	xM3S[e][b][i] = xS[i] + bM3[e][b];
	XM1S[e][b][i] = XS[i] + bM1[e][b];
	XM3S[e][b][i] = XS[i] + bM3[e][b];
      }
      gM1W[e][b] = new TGraph(2, xM1W[e][b], y);
      gM3W[e][b] = new TGraph(2, xM3W[e][b], y);
      GM1W[e][b] = new TGraph(2, XM1W[e][b], Y);
      GM3W[e][b] = new TGraph(2, XM3W[e][b], Y);
      fM1W[e][b] = new TF1(Form("%s%f%s%d", "wire, e1", eta[e], "b", b), "pol1");
      fM3W[e][b] = new TF1(Form("%s%f%s%d", "wire, e3", eta[e], "b", b), "pol1");
      FM1W[e][b] = new TF1(Form("%s%f%s%d", "wire, E1", eta[e], "B", b), "pol1");
      FM3W[e][b] = new TF1(Form("%s%f%s%d", "wire, E3", eta[e], "B", b), "pol1");
      gM1S[e][b] = new TGraph(2, xM1S[e][b], y);
      gM3S[e][b] = new TGraph(2, xM3S[e][b], y);
      GM1S[e][b] = new TGraph(2, XM1S[e][b], Y);
      GM3S[e][b] = new TGraph(2, XM3S[e][b], Y);
      fM1S[e][b] = new TF1(Form("%s%f%s%d", "strip, e1", eta[e], "b", b), "pol1");
      fM3S[e][b] = new TF1(Form("%s%f%s%d", "strip, e3", eta[e], "b", b), "pol1");
      FM1S[e][b] = new TF1(Form("%s%f%s%d", "strip, E1", eta[e], "B", b), "pol1");
      FM3S[e][b] = new TF1(Form("%s%f%s%d", "strip, E3", eta[e], "B", b), "pol1");

      gM1W[e][b]->Fit(Form("%s%f%s%d", "wire, e1", eta[e], "b", b));
      gM3W[e][b]->Fit(Form("%s%f%s%d", "wire, e3", eta[e], "b", b));
      GM1W[e][b]->Fit(Form("%s%f%s%d", "wire, E1", eta[e], "B", b));
      GM3W[e][b]->Fit(Form("%s%f%s%d", "wire, E3", eta[e], "B", b));
      gM1S[e][b]->Fit(Form("%s%f%s%d", "strip, e1", eta[e], "b", b));
      gM3S[e][b]->Fit(Form("%s%f%s%d", "strip, e3", eta[e], "b", b));
      GM1S[e][b]->Fit(Form("%s%f%s%d", "strip, E1", eta[e], "B", b));
      GM3S[e][b]->Fit(Form("%s%f%s%d", "strip, E3", eta[e], "B", b));

      
      if     (xM1W[e][b][1] <= Wwindow) {
	currM1W[e][b] = 1.;
      }
      else if(xM1W[e][b][0] < Wwindow && Wwindow <= xM1W[e][b][1]) {
	currM1W[e][b] = 1 - ( ( ( Wwindow * fM1W[e][b]->GetParameter(1) + fM1W[e][b]->GetParameter(0) ) * (xM1W[e][b][1] - Wwindow) ) / lengthW );
      }
      else if(XM1W[e][b][0] < Wwindow && Wwindow <= xM1W[e][b][0]) {
	currM1W[e][b] =       ( ( Wwindow * FM1W[e][b]->GetParameter(1) + FM1W[e][b]->GetParameter(0) ) * (Wwindow - XM1W[e][b][0]) ) / lengthW;
      }
      else if(Wwindow <= XM1W[e][b][0]) {
	currM1W[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM1W[e][b][1] <= BX) {
	nextM1W[e][b] = 0.;
      }
      else if(xM1W[e][b][0] < BX && BX <= xM1W[e][b][1]) {
	nextM1W[e][b] =     ( (   BX * fM1W[e][b]->GetParameter(1) + fM1W[e][b]->GetParameter(0) ) * (xM1W[e][b][1] - BX) ) / lengthW;
      }
      else if(XM1W[e][b][0] < BX && BX <= xM1W[e][b][0]) {
	nextM1W[e][b] = 1 - ( ( ( BX * FM1W[e][b]->GetParameter(1) + FM1W[e][b]->GetParameter(0) ) * (BX - XM1W[e][b][0]) ) / lengthW );
      }
      else if(BX <= XM1W[e][b][0]) {
	nextM1W[e][b] = 1.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM1W[e][b][1] <= (Wwindow + BX)) {
	nextM1W[e][b] -= 0.;
      }
      else if(xM1W[e][b][0] < (Wwindow + BX) && (Wwindow + BX) <= xM1W[e][b][1]) {
	nextM1W[e][b] -=     ( (   (Wwindow + BX) * fM1W[e][b]->GetParameter(1) + fM1W[e][b]->GetParameter(0) ) * (xM1W[e][b][1] - (Wwindow + BX)) ) / lengthW;
      }
      else if(XM1W[e][b][0] < (Wwindow + BX) && (Wwindow + BX) <= xM1W[e][b][0]) {
	nextM1W[e][b] -= 1 - ( ( ( (Wwindow + BX) * FM1W[e][b]->GetParameter(1) + FM1W[e][b]->GetParameter(0) ) * ((Wwindow + BX) - XM1W[e][b][0]) ) / lengthW );
      }
      else if((Wwindow + BX) <= XM1W[e][b][0]) {
	nextM1W[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM3W[e][b][1] <= Wwindow) {
	currM3W[e][b] = 1.;
      }
      else if(xM3W[e][b][0] < Wwindow && Wwindow <= xM3W[e][b][1]) {
	currM3W[e][b] = 1 - ( ( ( Wwindow * fM3W[e][b]->GetParameter(1) + fM3W[e][b]->GetParameter(0) ) * (xM3W[e][b][1] - Wwindow) ) / lengthW );
      }
      else if(XM3W[e][b][0] < Wwindow && Wwindow <= xM3W[e][b][0]) {
	currM3W[e][b] =       ( ( Wwindow * FM3W[e][b]->GetParameter(1) + FM3W[e][b]->GetParameter(0) ) * (Wwindow - XM3W[e][b][0]) ) / lengthW;
      }
      else if(Wwindow <= XM3W[e][b][0]) {
	currM3W[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM3W[e][b][1] <= BX) {
	nextM3W[e][b] = 0.;
      }
      else if(xM3W[e][b][0] < BX && BX <= xM3W[e][b][1]) {
	nextM3W[e][b] =     ( (   BX * fM3W[e][b]->GetParameter(1) + fM3W[e][b]->GetParameter(0) ) * (xM3W[e][b][1] - BX) ) / lengthW;
      }
      else if(XM3W[e][b][0] < BX && BX <= xM3W[e][b][0]) {
	nextM3W[e][b] = 1 - ( ( ( BX * FM3W[e][b]->GetParameter(1) + FM3W[e][b]->GetParameter(0) ) * (BX - XM3W[e][b][0]) ) / lengthW );
      }
      else if(BX <= XM3W[e][b][0]) {
	nextM3W[e][b] = 1.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM3W[e][b][1] <= (Wwindow + BX)) {
	nextM3W[e][b] -= 0.;
      }
      else if(xM3W[e][b][0] < (Wwindow + BX) && (Wwindow + BX) <= xM3W[e][b][1]) {
	nextM3W[e][b] -=     ( (   (Wwindow + BX) * fM3W[e][b]->GetParameter(1) + fM3W[e][b]->GetParameter(0) ) * (xM3W[e][b][1] - (Wwindow + BX)) ) / lengthW;
      }
      else if(XM3W[e][b][0] < (Wwindow + BX) && (Wwindow + BX) <= xM3W[e][b][0]) {
	nextM3W[e][b] -= 1 - ( ( ( (Wwindow + BX) * FM3W[e][b]->GetParameter(1) + FM3W[e][b]->GetParameter(0) ) * ((Wwindow + BX) - XM3W[e][b][0]) ) / lengthW );
      }
      else if((Wwindow + BX) <= XM3W[e][b][0]) {
	nextM3W[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;



      if     (xM1S[e][b][1] <= Swindow) {
	currM1S[e][b] = 1.;
      }
      else if(xM1S[e][b][0] < Swindow && Swindow <= xM1S[e][b][1]) {
	currM1S[e][b] = 1 - ( ( ( Swindow * fM1S[e][b]->GetParameter(1) + fM1S[e][b]->GetParameter(0) ) * (xM1S[e][b][1] - Swindow) ) / lengthS );
      }
      else if(XM1S[e][b][0] < Swindow && Swindow <= xM1S[e][b][0]) {
	currM1S[e][b] =       ( ( Swindow * FM1S[e][b]->GetParameter(1) + FM1S[e][b]->GetParameter(0) ) * (Swindow - XM1S[e][b][0]) ) / lengthS;
      }
      else if(Swindow <= XM1S[e][b][0]) {
	currM1S[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM1S[e][b][1] <= BX) {
	nextM1S[e][b] = 0.;
      }
      else if(xM1S[e][b][0] < BX && BX <= xM1S[e][b][1]) {
	nextM1S[e][b] =     ( (   BX * fM1S[e][b]->GetParameter(1) + fM1S[e][b]->GetParameter(0) ) * (xM1S[e][b][1] - BX) ) / lengthS;
      }
      else if(XM1S[e][b][0] < BX && BX <= xM1S[e][b][0]) {
	nextM1S[e][b] = 1 - ( ( ( BX * FM1S[e][b]->GetParameter(1) + FM1S[e][b]->GetParameter(0) ) * (BX - XM1S[e][b][0]) ) / lengthS );
      }
      else if(BX <= XM1S[e][b][0]) {
	nextM1S[e][b] = 1.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM1S[e][b][1] <= (Swindow + BX)) {
	nextM1S[e][b] -= 0.;
      }
      else if(xM1S[e][b][0] < (Swindow + BX) && (Swindow + BX) <= xM1S[e][b][1]) {
	nextM1S[e][b] -=     ( (   (Swindow + BX) * fM1S[e][b]->GetParameter(1) + fM1S[e][b]->GetParameter(0) ) * (xM1S[e][b][1] - (Swindow + BX)) ) / lengthS;
      }
      else if(XM1S[e][b][0] < (Swindow + BX) && (Swindow + BX) <= xM1S[e][b][0]) {
	nextM1S[e][b] -= 1 - ( ( ( (Swindow + BX) * FM1S[e][b]->GetParameter(1) + FM1S[e][b]->GetParameter(0) ) * ((Swindow + BX) - XM1S[e][b][0]) ) / lengthS );
      }
      else if((Swindow + BX) <= XM1S[e][b][0]) {
	nextM1S[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM3S[e][b][1] <= Swindow) {
	currM3S[e][b] = 1.;
      }
      else if(xM3S[e][b][0] < Swindow && Swindow <= xM3S[e][b][1]) {
	currM3S[e][b] = 1 - ( ( ( Swindow * fM3S[e][b]->GetParameter(1) + fM3S[e][b]->GetParameter(0) ) * (xM3S[e][b][1] - Swindow) ) / lengthS );
      }
      else if(XM3S[e][b][0] < Swindow && Swindow <= xM3S[e][b][0]) {
	currM3S[e][b] =       ( ( Swindow * FM3S[e][b]->GetParameter(1) + FM3S[e][b]->GetParameter(0) ) * (Swindow - XM3S[e][b][0]) ) / lengthS;
      }
      else if(Swindow <= XM3S[e][b][0]) {
	currM3S[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM3S[e][b][1] <= BX) {
	nextM3S[e][b] = 0.;
      }
      else if(xM3S[e][b][0] < BX && BX <= xM3S[e][b][1]) {
	nextM3S[e][b] =     ( (   BX * fM3S[e][b]->GetParameter(1) + fM3S[e][b]->GetParameter(0) ) * (xM3S[e][b][1] - BX) ) / lengthS;
      }
      else if(XM3S[e][b][0] < BX && BX <= xM3S[e][b][0]) {
	nextM3S[e][b] = 1 - ( ( ( BX * FM3S[e][b]->GetParameter(1) + FM3S[e][b]->GetParameter(0) ) * (BX - XM3S[e][b][0]) ) / lengthS );
      }
      else if(BX <= XM3S[e][b][0]) {
	nextM3S[e][b] = 1.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

      if     (xM3S[e][b][1] <= (Swindow + BX)) {
	nextM3S[e][b] -= 0.;
      }
      else if(xM3S[e][b][0] < (Swindow + BX) && (Swindow + BX) <= xM3S[e][b][1]) {
	nextM3S[e][b] -=     ( (   (Swindow + BX) * fM3S[e][b]->GetParameter(1) + fM3S[e][b]->GetParameter(0) ) * (xM3S[e][b][1] - (Swindow + BX)) ) / lengthS;
      }
      else if(XM3S[e][b][0] < (Swindow + BX) && (Swindow + BX) <= xM3S[e][b][0]) {
	nextM3S[e][b] -= 1 - ( ( ( (Swindow + BX) * FM3S[e][b]->GetParameter(1) + FM3S[e][b]->GetParameter(0) ) * ((Swindow + BX) - XM3S[e][b][0]) ) / lengthS );
      }
      else if((Swindow + BX) <= XM3S[e][b][0]) {
	nextM3S[e][b] = 0.;
      }
      else std::cout<<"ERROR:Unknown Value"<<std::endl;

    }
    //if(e == 0) canvasW[e]->SaveAs("wire_tri.pdf(");
    //canvasW[e]->SaveAs("wire_tri.pdf");
    //if(e == 14) canvasW[e]->SaveAs("wire_tri.pdf)");
    //if(e == 0) canvasS[e]->SaveAs("strip_tri.pdf(");
    //canvasS[e]->SaveAs("strip_tri.pdf");
    //if(e == 14) canvasS[e]->SaveAs("strip_tri.pdf)");

  }

  float currW[etanum][betanum];
  float nextW[etanum][betanum];
  float currS[etanum][betanum];
  float nextS[etanum][betanum];
  float curr[etanum][betanum];
  float next[etanum][betanum];
  TGraph *singW[etanum];
  TGraph *lateW[etanum];
  TGraph *singS[etanum];
  TGraph *lateS[etanum];
  TGraph *sing[etanum];
  TGraph *late[etanum];

  TCanvas *effcanvasW = new TCanvas(Form("%s","wire effciency"),Form("%s","wire efficiency"), 1800, 1000);
  effcanvasW->Divide(5, 3);
  TCanvas *effcanvasS = new TCanvas(Form("%s","strip effciency"),Form("%s","strip efficiency"), 1800, 1000);
  effcanvasS->Divide(5, 3);
  TCanvas *effcanvas  = new TCanvas(Form("%s","effciency"),Form("%s","efficiency"), 1800, 1000);
  effcanvas->Divide(5, 3);

  for(int e = 0;e < etanum;e++) {
    for(int b = 0;b < betanum;b++) {
      currW[e][b] = currM1W[e][b] * currM3W[e][b];
      nextW[e][b] = nextM1W[e][b] * nextM3W[e][b];
      currS[e][b] = currM1S[e][b] * currM3S[e][b];
      nextS[e][b] = nextM1S[e][b] * nextM3S[e][b];
      curr[e][b]  = currW[e][b]   * currS[e][b];
      next[e][b]  = nextW[e][b]   * nextS[e][b];
      //curr[e][b] = curr[e][b] * 0.8;
      //next[e][b] = next[e][b] * 0.3;
      if(e == 12) cout<<currW[e][b]<<"\t"<<nextW[e][b]<<endl;
    }
    singW[e] = new TGraph(betanum, ex, currW[e]); 
    lateW[e] = new TGraph(betanum, ex, nextW[e]); 
    singS[e] = new TGraph(betanum, ex, currS[e]); 
    lateS[e] = new TGraph(betanum, ex, nextS[e]); 
    sing[e]  = new TGraph(betanum, ex, curr[e] ); 
    late[e]  = new TGraph(betanum, ex, next[e] ); 
    singW[e]->SetName(Form("%s%d","singW",e));
    lateW[e]->SetName(Form("%s%d","lateW",e));
    singS[e]->SetName(Form("%s%d","singS",e));
    lateS[e]->SetName(Form("%s%d","lateS",e));
    sing[e]->SetName(Form("%s%d","sing",e));
    late[e]->SetName(Form("%s%d","late",e));
    effcanvasW->cd(e+1);
    singW[e]->SetTitle(Form("%s%f", "wire, | #eta | = ", eta[e]));
    singW[e]->SetLineColor(kRed);
    lateW[e]->SetLineColor(kBlue);
    singW[e]->GetXaxis()->SetLimits(0,1);
    lateW[e]->GetXaxis()->SetLimits(0,1);
    singW[e]->GetHistogram()->SetMinimum(0.);
    singW[e]->GetHistogram()->SetMaximum(1.1);
    singW[e]->Draw("AC");
    lateW[e]->Draw("C same");
    singW[e]->Write();
    lateW[e]->Write();

    effcanvasS->cd(e+1);
    singS[e]->SetTitle(Form("%s%f", "strip, | #eta | = ", eta[e]));
    singS[e]->SetLineColor(kRed);
    lateS[e]->SetLineColor(kBlue);
    singS[e]->GetXaxis()->SetLimits(0,1);
    lateS[e]->GetXaxis()->SetLimits(0,1);
    singS[e]->GetHistogram()->SetMinimum(0.);
    singS[e]->GetHistogram()->SetMaximum(1.1);
    singS[e]->Draw("AC");
    lateS[e]->Draw("C same");
    singS[e]->Write();
    lateS[e]->Write();

    effcanvas->cd(e+1);
    sing[e]->SetTitle(Form("%s%f", "| #eta | = ", eta[e]));
    sing[e]->SetLineColor(kRed);
    late[e]->SetLineColor(kBlue);
    sing[e]->GetXaxis()->SetLimits(0,1);
    sing[e]->GetHistogram()->SetMinimum(0.);
    sing[e]->GetHistogram()->SetMaximum(1.1);
    sing[e]->Draw("AC");
    late[e]->Draw("C same");
    sing[e]->Write();
    late[e]->Write();
  }
  //effcanvasW->SaveAs("eff.pdf(");
  //effcanvasS->SaveAs("eff.pdf");
  //effcanvas->SaveAs("eff1221.pdf");
  //file->Write();
}
