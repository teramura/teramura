//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul 19 20:13:53 2021 by ROOT version 6.22/08
// from TChain physics/physics
//////////////////////////////////////////////////////////

#ifndef plot2d_h
#define plot2d_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "string"
#include "vector"
#include "vector"
#include "vector"

class plot2d {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   string          *sample;
   vector<int>     *tagNumber;
   vector<int>     *probeNumber;
   vector<int>     *isStrip;
   vector<int>     *gasgap;
   vector<int>     *channel;
   vector<int>     *eta;
   vector<int>     *phi;
   vector<int>     *station;
   vector<int>     *bunch;
   vector<float>   *x;
   vector<float>   *y;
   vector<float>   *prdx;
   vector<float>   *prdy;
   vector<float>   *prdz;
   vector<float>   *dphi;
   vector<float>   *deta;
   vector<float>   *extpx;
   vector<float>   *extpy;
   vector<float>   *extpz;
   vector<float>   *exttheta;
   vector<float>   *thetaext;
   vector<float>   *charge;
   vector<float>   *mueta;
   vector<float>   *muphi;
   vector<float>   *mupt;
   vector<double>  *beta;
   vector<double>  *ext_p;
   vector<double>  *exteta;
   vector<double>  *extphi;

   // List of branches
   TBranch        *b_sample;   //!
   TBranch        *b_tagNumber;   //!
   TBranch        *b_probeNumber;   //!
   TBranch        *b_isStrip;   //!
   TBranch        *b_gasgap;   //!
   TBranch        *b_channel;   //!
   TBranch        *b_eta;   //!
   TBranch        *b_phi;   //!
   TBranch        *b_station;   //!
   TBranch        *b_bunch;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_prdx;   //!
   TBranch        *b_prdy;   //!
   TBranch        *b_prdz;   //!
   TBranch        *b_dphi;   //!
   TBranch        *b_deta;   //!
   TBranch        *b_extpx;   //!
   TBranch        *b_extpy;   //!
   TBranch        *b_extpz;   //!
   TBranch        *b_exttheta;   //!
   TBranch        *b_thetaext;   //!
   TBranch        *b_charge;   //!
   TBranch        *b_mueta;   //!
   TBranch        *b_muphi;   //!
   TBranch        *b_mupt;   //!
   TBranch        *b_beta;   //!
   TBranch        *b_ext_p;   //!
   TBranch        *b_exteta;   //!
   TBranch        *b_extphi;   //!

   plot2d(TTree *tree=0);
   virtual ~plot2d();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef plot2d_cxx
plot2d::plot2d(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("physics",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("physics","physics");
      //chain->Add("/gpfs/fs2001/teramura/Singlemu_0725_changetimingby2ns/alltuning.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/Singlemu_0803_stau/stau_alltuning.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/Singlemu_0803_stau/original.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/sample_0827/tuning_singlemu.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/sample_0827/original_singlemu.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/randomcrossbugfix/1025.root/physics");
      chain->Add("/gpfs/fs2001/teramura/master/mc_tune.root/physics");
      chain->Add("/gpfs/fs2001/teramura/master/mc_tune.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/periodBgetprd.root/physics");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

plot2d::~plot2d()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t plot2d::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t plot2d::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void plot2d::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   sample = 0;
   tagNumber = 0;
   probeNumber = 0;
   isStrip = 0;
   gasgap = 0;
   channel = 0;
   eta = 0;
   phi = 0;
   station = 0;
   bunch = 0;
   x = 0;
   y = 0;
   prdx = 0;
   prdy = 0;
   prdz = 0;
   dphi = 0;
   deta = 0;
   extpx = 0;
   extpy = 0;
   extpz = 0;
   exttheta = 0;
   thetaext = 0;
   charge = 0;
   mueta = 0;
   muphi = 0;
   mupt = 0;
   beta = 0;
   ext_p = 0;
   exteta = 0;
   extphi = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("sample", &sample, &b_sample);
   fChain->SetBranchAddress("tagNumber", &tagNumber, &b_tagNumber);
   fChain->SetBranchAddress("probeNumber", &probeNumber, &b_probeNumber);
   fChain->SetBranchAddress("isStrip", &isStrip, &b_isStrip);
   fChain->SetBranchAddress("gasgap", &gasgap, &b_gasgap);
   fChain->SetBranchAddress("channel", &channel, &b_channel);
   fChain->SetBranchAddress("eta", &eta, &b_eta);
   fChain->SetBranchAddress("phi", &phi, &b_phi);
   fChain->SetBranchAddress("station", &station, &b_station);
   fChain->SetBranchAddress("bunch", &bunch, &b_bunch);
   fChain->SetBranchAddress("x", &x, &b_x);
   fChain->SetBranchAddress("y", &y, &b_y);
   fChain->SetBranchAddress("prdx", &prdx, &b_prdx);
   fChain->SetBranchAddress("prdy", &prdy, &b_prdy);
   fChain->SetBranchAddress("prdz", &prdz, &b_prdz);
   fChain->SetBranchAddress("dphi", &dphi, &b_dphi);
   fChain->SetBranchAddress("deta", &deta, &b_deta);
   fChain->SetBranchAddress("extpx", &extpx, &b_extpx);
   fChain->SetBranchAddress("extpy", &extpy, &b_extpy);
   fChain->SetBranchAddress("extpz", &extpz, &b_extpz);
   fChain->SetBranchAddress("exttheta", &exttheta, &b_exttheta);
   fChain->SetBranchAddress("thetaext", &thetaext, &b_thetaext);
   fChain->SetBranchAddress("charge", &charge, &b_charge);
   fChain->SetBranchAddress("mueta", &mueta, &b_mueta);
   fChain->SetBranchAddress("muphi", &muphi, &b_muphi);
   fChain->SetBranchAddress("mupt", &mupt, &b_mupt);
   fChain->SetBranchAddress("beta", &beta, &b_beta);
   fChain->SetBranchAddress("ext_p", &ext_p, &b_ext_p);
   fChain->SetBranchAddress("exteta", &exteta, &b_exteta);
   fChain->SetBranchAddress("extphi", &extphi, &b_extphi);
   Notify();
}

Bool_t plot2d::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void plot2d::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t plot2d::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef plot2d_cxx
