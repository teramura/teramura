#define plot_cxx
#include "plot.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void plot::Loop()
{
  std::string PDF = "tmp";
  TFile *file;
  file = new TFile("notwist.root","recreate");

  // 21
  int STA[21] = {41,42,42,42,42,43,44,44,44,44,44,45,46,46,46,46,46,47,47,48,48};
  int ETA[21] = { 1, 1, 2, 3, 4, 1, 1, 2, 3, 4, 5, 1, 1, 2, 3, 4, 5, 1, 1, 1, 1};
  int PHI[21] = {24,48,48,48,48,24,48,48,48,48,48,24,48,48,48,48,48,24,24,21,21};

  float dPHI[21][2] = {{0.133,0.008},{0.070,0.007},{0.070,0.004},{0.071,0.003},{0.070,0.005},
		       {0.138,0.007},{0.072,0.006},{0.071,0.006},{0.071,0.005},{0.069,0.004},{0.067,0.004},
		       {0.133,0.005},{0.070,0.004},{0.070,0.003},{0.069,0.003},{0.068,0.003},{0.067,0.003},
		       {0.132,0.007},{0.132,0.007},{0.080,0.004},{0.080,0.004}};

  float dETA[21][2] = {{0.008,0.28},{0.0040,0.17},{0.0040,0.13},{0.0038,0.055},{0.0035,0.07},
		       {0.007,0.20},{0.0040,0.17},{0.0032,0.16},{0.0035,0.070},{0.0035,0.07},{0.0030,0.05},
		       {0.007,0.21},{0.0035,0.17},{0.0027,0.16},{0.0035,0.070},{0.0030,0.07},{0.0023,0.05},
		       {0.014,0.25},{0.014 ,0.25},{0.0070,0.09},{0.0070,0.09}};

  std::string hname[21] = {"M1 T1",
			   "M1 T3","M1 T6","M1 T7","M1 T8",
			   "M2 T2",
			   "M2 T4","M2 T6","M2 T7","M2 T8","M2 T9",
			   "M3 T2",
			   "M3 T5","M3 T6","M3 T7","M3 T8","M3 T9",
			   "T10 long","T10 short",
			   "T11 long","T11 short"};

  std::string sname[2] = {"wire channel","strip channel"};

  int nchannel[21][2] = {{105,32},
			 {92, 32},{61, 32},{23, 32},{24, 32},
			 {125,32},
			 {110,32},{103,32},{32, 32},{32, 32},{32, 32},
			 {122,32},
			 {96, 32},{106,32},{32, 32},{30, 32},{31, 32},
			 {32, 32},{32, 32},
			 {24, 32},{16, 32}};

  float wasd[21][8] = {{3.4 , 19.5,35.7,51.9,68.0,84.2,100.3,    0},
		       {8.3 , 23.3,38.4,53.5,68.6,83.7,    0,    0},{7.9 ,22.9,37.9,52.9,   0,   0,   0,0},{5.9,17.1,0,0,0,0,0,0},{6.1,17.8,0,0,0,0,0,0},
		       {7.3 , 22.9,38.4,54.0,69.6,85.1,100.7,116.3},
		       {7.3 , 22.7,38.2,53.7,69.2,84.7,100.1,    0},{7.9 ,22.4,36.9,51.4,65.8,80.3,94.8,0},{8.2,23.8,0,0,0,0,0,0},{8.2,23.8,0,0,0,0,0,0},{8.2,23.8,0,0,0,0,0,0},
		       {7.1 , 22.3,37.5,52.7,67.9,83.1, 98.3,113.5},
		       {8.1 , 23.7,39.4,55.1,70.8,86.4,    0,    0},{8.1 ,23.0,38.0,52.9,67.8,82.7,97.6,0},{8.2,23.8,0,0,0,0,0,0},{7.7,22.3,0,0,0,0,0,0},{8.0,23.0,0,0,0,0,0,0},
		       {28.0, 30.6,   0,   0,   0,   0,    0,    0},{27.9,30.5,   0,   0,   0,   0,   0,0},
		       {19.2, 21.8,   0,   0,   0,   0,    0,    0},{5.4 ,   0,   0,   0,   0,   0,   0,0}};

  float sasd[21][2] = {{8.3, 23.7},
		       {8.4, 23.6},{8.3, 23.7},{8.2, 23.8},{8.2, 23.8},
		       {8.4, 23.6},
		       {8.3, 23.6},{8.3, 23.7},{8.2, 23.8},{8.2, 23.8},{8.2, 23.8},
		       {8.4, 23.6},
		       {8.9, 23.1},{8.3, 23.7},{8.2, 23.8},{8.2, 23.8},{8.2, 23.8},
		       {8.2, 23.8},{7.0, 25.0},
		       {9.1, 22.9},{6.2, 25.8}};

  TArrow *lw[21][8];
  TArrow *ls[21][2];
  TLine *ll[8];

  std::cout<<"\033[34m"<<"Initialize the Histogram"<<std::endl;  

  TCanvas *c1[21][2];
  TH1D *hist[21][2];
  TH1D *ehist[21][2];
  TH1D *thist[21][2];

  TCanvas *d1[21][2];
  TH1D *hist_A[2];
  TH1D *ehist_A[2];
  TH1D *thist_A[2];
  TH1D *hist_C[2];
  TH1D *ehist_C[2];
  TH1D *thist_C[2];
  TH1D *hist_Al[2][3];
  TH1D *ehist_Al[2][3];
  TH1D *thist_Al[2][3];
  TH1D *hist_Cl[2][3];
  TH1D *ehist_Cl[2][3];
  TH1D *thist_Cl[2][3];
  TH1D *hist_AT[2];
  TH1D *ehist_AT[2];
  TH1D *thist_AT[2];

  for(int x = 0;x < 21;x++) {
    for(int s = 0;s < 2;s++) {
      d1[x][s] = new TCanvas (Form("%d%s%d",s,"_d1_",x),Form("%d%s%d",s,"_d1_",x));
      if(x == 0) {
	hist_A[s] = new TH1D(Form("%s%d","hist_A",s),Form("%s%d","hist_A",s),21,0.5,21.5);
	ehist_A[s] = new TH1D(Form("%s%d","ehist_A",s),Form("%s%d","ehist_A",s),21,0.5,21.5);
	thist_A[s] = new TH1D(Form("%s%d","thist_A",s),Form("%s%d","thist_A",s),21,0.5,21.5);
	hist_C[s] = new TH1D(Form("%s%d","hist_C",s),Form("%s%d","hist_C",s),21,0.5,21.5);
	ehist_C[s] = new TH1D(Form("%s%d","ehist_C",s),Form("%s%d","ehist_C",s),21,0.5,21.5);
	thist_C[s] = new TH1D(Form("%s%d","thist_C",s),Form("%s%d","thist_C",s),21,0.5,21.5);
	hist_AT[s] = new TH1D(Form("%s%d","hist_AT",s),Form("%s%d","hist_AT",s),24,1.,25);
	ehist_AT[s] = new TH1D(Form("%s%d","ehist_AT",s),Form("%s%d","ehist_AT",s),24,1.,25);
	thist_AT[s] = new TH1D(Form("%s%d","thist_AT",s),Form("%s%d","thist_AT",s),24,1.,25);
      }
      if(x < 3) {
	hist_Al[s][x] = new TH1D(Form("%d%s%d",x,"hist_Al",s),Form("%d%s%d",x,"hist_Al",s),21,0.5,21.5);
	ehist_Al[s][x] = new TH1D(Form("%d%s%d",x,"ehist_Al",s),Form("%d%s%d",x,"ehist_Al",s),21,0.5,21.5);
	thist_Al[s][x] = new TH1D(Form("%d%s%d",x,"thist_Al",s),Form("%d%s%d",x,"thist_Al",s),21,0.5,21.5);
	hist_Cl[s][x] = new TH1D(Form("%d%s%d",x,"hist_Cl",s),Form("%d%s%d",x,"hist_Cl",s),21,0.5,21.5);
	ehist_Cl[s][x] = new TH1D(Form("%d%s%d",x,"ehist_Cl",s),Form("%d%s%d",x,"ehist_Cl",s),21,0.5,21.5);
	thist_Cl[s][x] = new TH1D(Form("%d%s%d",x,"thist_Cl",s),Form("%d%s%d",x,"thist_Cl",s),21,0.5,21.5);
      }
      if(x < 21) {
       	c1[x][s] = new TCanvas (Form("%s%s%s","c1",hname[x].c_str(),sname[s].c_str()),Form("%s%s%s","c1",hname[x].c_str(),sname[s].c_str()));
	hist[x][s] = new TH1D (Form("%s%s",hname[x].c_str(),sname[s].c_str()),Form("%s%s",hname[x].c_str(),sname[s].c_str()),nchannel[x][s],0.5,nchannel[x][s]+0.5);
	ehist[x][s] = new TH1D (Form("%s%s%s","events ",hname[x].c_str(),sname[s].c_str()),Form("%s%s%s","events ",hname[x].c_str(),sname[s].c_str()),nchannel[x][s],0.5,nchannel[x][s]+0.5);
	thist[x][s] = new TH1D (Form("%s%s%s","timing ",hname[x].c_str(),sname[s].c_str()),Form("%s%s%s","timing ",hname[x].c_str(),sname[s].c_str()),nchannel[x][s],0.5,nchannel[x][s]+0.5);
      }
    }
    for(int w = 0;w < 8;w++) {
      if(wasd[x][w] != 0) lw[x][w]  = new TArrow(wasd[x][w],0,wasd[x][w],0.25,0.03,"-<-");
      else continue;
      lw[x][w]->SetAngle(60);
      lw[x][w]->SetLineWidth(2);
    }
    for(int t = 0;t < 2;t++) {
      if(sasd[x][t] != 0) ls[x][t]  = new TArrow(sasd[x][t],0,sasd[x][t],0.25,0.03,"-<-");
      else continue;
      ls[x][t]->SetAngle(60);
      ls[x][t]->SetLineWidth(2);
    }
  }
  for(int l = 0;l < 8;l++) {
    ll[l]  = new TLine(16*l,0,16*l,0.5);
    ll[l]->SetLineStyle(3);
  }

  std::string mark = "#";

  if (fChain == 0) return;
  Long64_t nentries = fChain->GetEntriesFast();
  Long64_t nbytes = 0, nb = 0;
  std::cout<<"\033[34m"<<"Start the Looping Area"<<std::endl;  
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;

    //if(jentry == 1000) break;
    if(jentry%10000 == 0) mark += "#";
    if(jentry%1000 == 0) {
      std::cout<<"\033[33m"<<mark<<" ["<<jentry<<" events]"<<"\033[m"<<"\r";
    }

    for(int i = 0;i < station->size();i++){

      if(eta->at(i)<0) continue;
      if(gasgap->at(i)!=1) continue;

      int trueX = 99;
      for(int x = 0;x < 21;x++) {
	if( station->at(i)   != STA[x] ) continue;
	if( fabs(eta->at(i)) != ETA[x] ) continue;
	if( x == 17 && phi->at(i)%3 == 2) continue; // long
	if( x == 18 && phi->at(i)%3 != 2) continue; // short
	if( x == 19 && (phi->at(i) == 2  || phi->at(i) == 11 || phi->at(i) == 13 || phi->at(i) == 14 ||
			phi->at(i) == 15 || phi->at(i) == 19 || phi->at(i) == 20 || phi->at(i) == 21)) continue; // 24 channel
	if( x == 20 && (phi->at(i) != 2  && phi->at(i) != 11 && phi->at(i) != 13 && phi->at(i) != 14 &&
			phi->at(i) != 15 && phi->at(i) != 19 && phi->at(i) != 20 && phi->at(i) != 21)) continue; // 16 channel
	trueX = x;
      }
      if(trueX == 99) continue;

      if(fabs(dphi->at(i)) >= dPHI[trueX][isStrip->at(i)]) continue;
      if(fabs(deta->at(i)) >= dETA[trueX][isStrip->at(i)]) continue;

      float bunchbin = 99.;

      if      ((i != 0) &&
	      (bunch->at(i)==3 && bunch->at(i-1)==2 && 
	       probeNumber->at(i)==probeNumber->at(i-1) && 
	       eta->at(i)==eta->at(i-1) && 
	       phi->at(i)==phi->at(i-1) && 
	       gasgap->at(i)==gasgap->at(i-1) && 
	       channel->at(i)==channel->at(i-1))) bunchbin = 0.5;

      else if((i != 0) &&
	      (bunch->at(i)==2 && bunch->at(i-1)==1 && 
	       probeNumber->at(i)==probeNumber->at(i-1) && 
	       eta->at(i)==eta->at(i-1) && 
	       phi->at(i)==phi->at(i-1) && 
	       gasgap->at(i)==gasgap->at(i-1) && 
	       channel->at(i)==channel->at(i-1))) bunchbin = 0.;
      
      else if(((bunch->size() > i+1)) &&
	      ((bunch->at(i+1)==3 && bunch->at(i)==2 && 
	       probeNumber->at(i+1)==probeNumber->at(i) && 
	       eta->at(i+1)==eta->at(i) && 
	       phi->at(i+1)==phi->at(i) && 
	       gasgap->at(i+1)==gasgap->at(i) && 
	       channel->at(i+1)==channel->at(i)) ||
	      (bunch->at(i+1)==2 && bunch->at(i)==1 && 
	       probeNumber->at(i+1)==probeNumber->at(i) && 
	       eta->at(i+1)==eta->at(i) && 
	       phi->at(i+1)==phi->at(i) && 
	       gasgap->at(i+1)==gasgap->at(i) && 
	       channel->at(i+1)==channel->at(i)))) bunchbin = 99;
      
      else if(((i == 0  && bunch->size() > 1)) &&
	      ((bunch->at(i+1)==3 && bunch->at(i)==2 && 
	       probeNumber->at(i+1)==probeNumber->at(i) && 
	       eta->at(i+1)==eta->at(i) && 
	       phi->at(i+1)==phi->at(i) && 
	       gasgap->at(i+1)==gasgap->at(i) && 
	       channel->at(i+1)==channel->at(i)) ||
	      (bunch->at(i+1)==2 && bunch->at(i)==1 && 
	       probeNumber->at(i+1)==probeNumber->at(i) && 
	       eta->at(i+1)==eta->at(i) && 
	       phi->at(i+1)==phi->at(i) && 
	       gasgap->at(i+1)==gasgap->at(i) && 
	       channel->at(i+1)==channel->at(i)))) bunchbin = 99;
      
      else {
	if(bunch->at(i) == 1) bunchbin = 0.;
	if(bunch->at(i) == 2) bunchbin = 0.;
	if(bunch->at(i) == 3) bunchbin = 1.;
      }

      if(bunchbin == 99) continue;

      hist[trueX][isStrip->at(i)]->Fill(channel->at(i),bunchbin);
      ehist[trueX][isStrip->at(i)]->Fill(channel->at(i),1.);

      if(eta->at(i) > 0) {
	hist_A[isStrip->at(i)]->Fill(trueX + 1,bunchbin);
	ehist_A[isStrip->at(i)]->Fill(trueX + 1,1.);
	hist_Al[isStrip->at(i)][gasgap->at(i)-1]->Fill(trueX + 1,bunchbin);
	ehist_Al[isStrip->at(i)][gasgap->at(i)-1]->Fill(trueX + 1,1.);
      }
      else if(eta->at(i) < 0) {
	hist_C[isStrip->at(i)]->Fill(trueX + 1,bunchbin);
	ehist_C[isStrip->at(i)]->Fill(trueX + 1,1.);
	hist_Cl[isStrip->at(i)][gasgap->at(i)-1]->Fill(trueX + 1,bunchbin);
	ehist_Cl[isStrip->at(i)][gasgap->at(i)-1]->Fill(trueX + 1,1.);
      }

      if(station->at(i) == 48 && eta->at(i) > 0 && gasgap->at(i) == 1) {
	hist_AT[isStrip->at(i)]->Fill(phi->at(i),bunchbin);
	ehist_AT[isStrip->at(i)]->Fill(phi->at(i),1.);
      }

    } //i loop
  } //all entry loop

  std::cout<<std::endl;
  std::cout<<"\033[34m"<<"Start drawing histogram"<<std::endl;

  TLatex *latex=new TLatex();
  TLegend *leg[21][2];

  for(int x = 0;x < 21;x++) {
    for(int s = 0;s < 2;s++) {
      thist_A[s]->GetXaxis()->SetBinLabel(x+1,hname[x].c_str());
      thist_C[s]->GetXaxis()->SetBinLabel(x+1,hname[x].c_str());
      thist_Al[s][0]->GetXaxis()->SetBinLabel(x+1,hname[x].c_str());
      thist_Cl[s][0]->GetXaxis()->SetBinLabel(x+1,hname[x].c_str());
      thist[x][s]->Divide(hist[x][s],ehist[x][s],1,1,"B");
      for(int f = 0;f < thist[x][s]->GetNbinsX();f++) {
	thist[x][s]->Fill(1+f,0.000001);
      }
      
      c1[x][s]->cd();
      c1[x][s]->SetTicks();
      c1[x][s]->SetLeftMargin(0.15);
      c1[x][s]->SetBottomMargin(0.1);
      thist[x][s]->SetTitle("");
      thist[x][s]->GetXaxis()->SetTitle("channel");
      thist[x][s]->GetYaxis()->SetTitle("P_{timing}");
      thist[x][s]->SetLabelSize(0.06,"x");
      thist[x][s]->SetLabelSize(0.06,"y");
      thist[x][s]->SetTitleSize(0.06,"x");
      thist[x][s]->SetTitleSize(0.06,"y");
      thist[x][s]->GetXaxis()->SetTitleOffset(0.7);
      thist[x][s]->GetYaxis()->SetTitleOffset(1.1);
      thist[x][s]->SetMarkerStyle(8);
      thist[x][s]->GetXaxis()->SetRangeUser(0.,nchannel[x][s]);
      thist[x][s]->GetYaxis()->SetRangeUser(0.,0.5);
      thist[x][s]->SetStats(0);
      thist[x][s]->Draw();

      leg[x][s] = new TLegend(0.65,0.69,0.9,0.90);
      leg[x][s]->SetBorderSize(0);
      leg[x][s]->SetFillStyle(0);
      //leg[x][s]->AddEntry(thist[x][s],"Run-2","lp");

      for(int l = 0;l < 8;l++) {
	if(nchannel[x][s] < 16*l) break;
	ll[l]->Draw();
      }

      if(s == 0) {
	for(int w = 0;w < 8;w++) {
	  if(w == 0) leg[x][s]->AddEntry(lw[x][w],"ASD position","l");
	  if(wasd[x][w] != 0) lw[x][w]->Draw();
	}
      }
      if(s == 1) {
	for(int t = 0;t < 2;t++) {
	  if(t == 0) leg[x][s]->AddEntry(ls[x][t],"ASD position","l");
	  if(sasd[x][t] != 0) ls[x][t]->Draw();
	}
      }

      latex->SetNDC(1);
      latex->SetTextFont(72);
      latex->SetTextSize(0.05);
      latex->DrawLatex(0.20,0.83,"ATLAS");
      latex->SetTextFont(42);
      latex->DrawLatex(0.33,0.83,"Work In Progress");
      latex->DrawLatex(0.20,0.78,"2018 Run-2 period B");
      latex->DrawLatex(0.20,0.72,hname[x].c_str());
      latex->DrawLatex(0.20,0.66,sname[s].c_str());

      leg[x][s]->Draw();
      
      //if(x == 0 && s == 0) c1[x][s]->SaveAs(Form("%s%s",PDF.c_str(),".pdf("));
      //c1[x][s]->SaveAs(Form("%s%s",PDF.c_str(),".pdf"));
      //if(x == 20 && s == 1) c1[x][s]->SaveAs(Form("%s%s",PDF.c_str(),".pdf)"));
    }
  }

  /*
  TLegend *leg1;
  leg1 = new TLegend(0.65,0.69,0.9,0.90);
  leg1->SetBorderSize(0);
  leg1->SetFillStyle(0);
  leg1->AddEntry(thist_A[0],"A-Side","lp");
  leg1->AddEntry(thist_C[0],"C-Side","lp");
  
  for(int s = 0;s < 2;s++) {
      thist_A[s]->Divide(hist_A[s],ehist_A[s],1,1,"B");
      thist_C[s]->Divide(hist_C[s],ehist_C[s],1,1,"B");
      d1[0][s]->cd();
      d1[0][s]->SetTicks();
      d1[0][s]->SetLeftMargin(0.15);
      d1[0][s]->SetBottomMargin(0.1);
      thist_A[s]->SetTitle("");
      thist_A[s]->GetYaxis()->SetTitle("P_{timing}");
      thist_A[s]->SetLabelSize(0.04,"x");
      thist_A[s]->SetLabelSize(0.06,"y");
      thist_A[s]->SetTitleSize(0.04,"x");
      thist_A[s]->SetTitleSize(0.06,"y");
      thist_A[s]->GetXaxis()->SetTitleOffset(0.7);
      thist_A[s]->GetYaxis()->SetTitleOffset(1.1);
      thist_A[s]->SetMarkerStyle(20);
      thist_C[s]->SetMarkerStyle(21);
      thist_A[s]->SetMarkerColor(kRed-7);
      thist_C[s]->SetMarkerColor(kBlue-4);
      thist_A[s]->SetLineColor(kRed-7);
      thist_C[s]->SetLineColor(kBlue-4);
      thist_A[s]->GetYaxis()->SetRangeUser(0.,0.5);
      thist_A[s]->SetStats(0);
      thist_A[s]->Draw();
      thist_C[s]->Draw("same");
      latex->SetNDC(1);
      latex->SetTextFont(72);
      latex->SetTextSize(0.05);
      latex->DrawLatex(0.20,0.83,"ATLAS");
      latex->SetTextFont(42);
      latex->DrawLatex(0.33,0.83,"Work In Progress");
      latex->DrawLatex(0.20,0.76,sname[s].c_str());
      leg1->Draw();
    }


  TLegend *leg2;
  leg2 = new TLegend(0.65,0.69,0.9,0.90);
  leg2->SetBorderSize(0);
  leg2->SetFillStyle(0);
  leg2->AddEntry(thist_Al[0][0],"layer 1","lp");
  leg2->AddEntry(thist_Al[0][1],"layer 2","lp");
  leg2->AddEntry(thist_Al[0][2],"layer 3","lp");
  
  for(int s = 0;s < 2;s++) {
    d1[1][s]->cd();
    d1[1][s]->SetTicks();
    d1[1][s]->SetLeftMargin(0.15);
    d1[1][s]->SetBottomMargin(0.1);
    for(int x = 0;x < 3;x++) {
      thist_Al[s][x]->Divide(hist_Al[s][x],ehist_Al[s][x],1,1,"B");
    }
    thist_Al[s][0]->SetTitle("");
    thist_Al[s][0]->GetYaxis()->SetTitle("P_{timing}");
    thist_Al[s][0]->SetLabelSize(0.04,"x");
    thist_Al[s][0]->SetLabelSize(0.06,"y");
    thist_Al[s][0]->SetTitleSize(0.04,"x");
    thist_Al[s][0]->SetTitleSize(0.06,"y");
    thist_Al[s][0]->GetXaxis()->SetTitleOffset(0.7);
    thist_Al[s][0]->GetYaxis()->SetTitleOffset(1.1);
    thist_Al[s][0]->GetYaxis()->SetRangeUser(0.,0.5);
    thist_Al[s][0]->SetStats(0);
    thist_Al[s][0]->SetMarkerStyle(20);
    thist_Al[s][0]->SetMarkerColor(kRed-7);
    thist_Al[s][0]->SetLineColor(kRed-7);
    thist_Al[s][1]->SetMarkerStyle(20);
    thist_Al[s][1]->SetMarkerColor(kBlue-4);
    thist_Al[s][1]->SetLineColor(kBlue-4);
    thist_Al[s][2]->SetMarkerStyle(20);
    thist_Al[s][2]->SetMarkerColor(kGreen+2);
    thist_Al[s][2]->SetLineColor(kGreen+2);
    thist_Al[s][0]->Draw();
    thist_Al[s][1]->Draw("same");
    thist_Al[s][2]->Draw("same");
    latex->SetNDC(1);
    latex->SetTextFont(72);
    latex->SetTextSize(0.05);
    latex->DrawLatex(0.20,0.83,"ATLAS");
    latex->SetTextFont(42);
    latex->DrawLatex(0.33,0.83,"Work In Progress");
    latex->DrawLatex(0.20,0.76,Form("%s%s",sname[s].c_str()," A-Side"));
    leg2->Draw();
  }
  

  for(int s = 0;s < 2;s++) {
    d1[2][s]->cd();
    d1[2][s]->SetTicks();
    d1[2][s]->SetLeftMargin(0.15);
    d1[2][s]->SetBottomMargin(0.1);
    for(int x = 0;x < 3;x++) {
      thist_Cl[s][x]->Divide(hist_Cl[s][x],ehist_Cl[s][x],1,1,"B");
    }
    thist_Cl[s][0]->SetTitle("");
    thist_Cl[s][0]->GetYaxis()->SetTitle("P_{timing}");
    thist_Cl[s][0]->SetLabelSize(0.04,"x");
    thist_Cl[s][0]->SetLabelSize(0.06,"y");
    thist_Cl[s][0]->SetTitleSize(0.04,"x");
    thist_Cl[s][0]->SetTitleSize(0.06,"y");
    thist_Cl[s][0]->GetXaxis()->SetTitleOffset(0.7);
    thist_Cl[s][0]->GetYaxis()->SetTitleOffset(1.1);
    thist_Cl[s][0]->GetYaxis()->SetRangeUser(0.,0.5);
    thist_Cl[s][0]->SetStats(0);
    thist_Cl[s][0]->SetMarkerStyle(20);
    thist_Cl[s][0]->SetMarkerColor(kRed-7);
    thist_Cl[s][0]->SetLineColor(kRed-7);
    thist_Cl[s][1]->SetMarkerStyle(20);
    thist_Cl[s][1]->SetMarkerColor(kBlue-4);
    thist_Cl[s][1]->SetLineColor(kBlue-4);
    thist_Cl[s][2]->SetMarkerStyle(20);
    thist_Cl[s][2]->SetMarkerColor(kGreen+2);
    thist_Cl[s][2]->SetLineColor(kGreen+2);
    thist_Cl[s][0]->Draw();
    thist_Cl[s][1]->Draw("same");
    thist_Cl[s][2]->Draw("same");
    latex->SetNDC(1);
    latex->SetTextFont(72);
    latex->SetTextSize(0.05);
    latex->DrawLatex(0.20,0.83,"ATLAS");
    latex->SetTextFont(42);
    latex->DrawLatex(0.33,0.83,"Work In Progress");
    latex->DrawLatex(0.20,0.76,Form("%s%s",sname[s].c_str()," C-Side"));
    leg2->Draw();
  }
  */
  for(int s = 0;s < 2;s++) {
    d1[0][s]->cd();
    d1[0][s]->SetTicks();
    d1[0][s]->SetLeftMargin(0.15);
    d1[0][s]->SetBottomMargin(0.1);
    thist_AT[s]->Divide(hist_AT[s],ehist_AT[s],1,1,"B");
    thist_AT[s]->SetTitle("");
    thist_AT[s]->GetYaxis()->SetTitle("P_{timing}");
    thist_AT[s]->SetLabelSize(0.04,"x");
    thist_AT[s]->SetLabelSize(0.06,"y");
    thist_AT[s]->SetTitleSize(0.04,"x");
    thist_AT[s]->SetTitleSize(0.06,"y");
    thist_AT[s]->GetXaxis()->SetTitleOffset(0.7);
    thist_AT[s]->GetYaxis()->SetTitleOffset(1.1);
    thist_AT[s]->GetYaxis()->SetRangeUser(0.,0.5);
    thist_AT[s]->SetStats(0);
    thist_AT[s]->SetMarkerStyle(20);
    thist_AT[s]->SetMarkerColor(kRed-7);
    thist_AT[s]->SetLineColor(kRed-7);
    thist_AT[s]->Draw();
    latex->SetNDC(1);
    latex->SetTextFont(72);
    latex->SetTextSize(0.05);
    latex->DrawLatex(0.20,0.83,"ATLAS");
    latex->SetTextFont(42);
    latex->DrawLatex(0.33,0.83,"Work In Progress");
  }


  file->Write();
}

