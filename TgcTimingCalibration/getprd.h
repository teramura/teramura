//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jul 31 17:45:49 2020 by ROOT version 6.20/06
// from TChain physics/
//////////////////////////////////////////////////////////

#ifndef getprd_h
#define getprd_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class getprd {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           RunNumber;
   Int_t           EventNumber;
   Int_t           lbn;
   Int_t           bcid;
   Int_t           detmask0;
   Int_t           detmask1;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   Int_t           pixelFlags;
   Int_t           sctFlags;
   Int_t           trtFlags;
   Int_t           larFlags;
   Int_t           tileFlags;
   Int_t           muonFlags;
   Int_t           fwdFlags;
   Int_t           coreFlags;
   Int_t           pixelError;
   Int_t           sctError;
   Int_t           trtError;
   Int_t           larError;
   Int_t           tileError;
   Int_t           muonError;
   Int_t           fwdError;
   Int_t           coreError;
   vector<float>   *vxp_x;
   vector<float>   *vxp_y;
   vector<float>   *vxp_z;
   vector<float>   *vxp_chi2;
   vector<int>     *vxp_ndof;
   vector<int>     *vxp_nTracks;
   vector<float>   *TGC_prd_x;
   vector<float>   *TGC_prd_y;
   vector<float>   *TGC_prd_z;
   vector<float>   *TGC_prd_shortWidth;
   vector<float>   *TGC_prd_longWidth;
   vector<float>   *TGC_prd_length;
   vector<int>     *TGC_prd_isStrip;
   vector<int>     *TGC_prd_gasGap;
   vector<int>     *TGC_prd_channel;
   vector<int>     *TGC_prd_eta;
   vector<int>     *TGC_prd_phi;
   vector<int>     *TGC_prd_station;
   vector<int>     *TGC_prd_bunch;
   vector<float>   *trig_L1_mu_eta;
   vector<float>   *trig_L1_mu_phi;
   vector<string>  *trig_L1_mu_thrName;
   vector<short>   *trig_L1_mu_thrNumber;
   vector<short>   *trig_L1_mu_RoINumber;
   vector<short>   *trig_L1_mu_sectorAddress;
   vector<int>     *trig_L1_mu_firstCandidate;
   vector<int>     *trig_L1_mu_moreCandInRoI;
   vector<int>     *trig_L1_mu_moreCandInSector;
   vector<short>   *trig_L1_mu_source;
   vector<short>   *trig_L1_mu_hemisphere;
   vector<short>   *trig_L1_mu_charge;
   vector<int>     *trig_L1_mu_vetoed;
   vector<string>  *trigger_info_chain;
   vector<int>     *trigger_info_isPassed;
   vector<vector<int> > *trigger_info_typeVec;
   vector<vector<float> > *trigger_info_ptVec;
   vector<vector<float> > *trigger_info_etaVec;
   vector<vector<float> > *trigger_info_phiVec;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_m;
   vector<int>     *mu_charge;
   vector<int>     *mu_author;
   vector<unsigned short> *mu_allAuthors;
   vector<int>     *mu_muonType;
   vector<float>   *mu_etcone20;
   vector<float>   *mu_etcone30;
   vector<float>   *mu_etcone40;
   vector<float>   *mu_ptcone20;
   vector<float>   *mu_ptcone30;
   vector<float>   *mu_ptcone40;
   vector<float>   *mu_trackfitchi2;
   vector<float>   *mu_trackfitndof;
   vector<float>   *mu_msInnerMatchChi2;
   vector<float>   *mu_msOuterMatchChi2;
   vector<int>     *mu_msInnerMatchDOF;
   vector<int>     *mu_msOuterMatchDOF;
   vector<int>     *mu_nOutliersOnTrack;
   vector<int>     *mu_nBLHits;
   vector<int>     *mu_nPixHits;
   vector<int>     *mu_nSCTHits;
   vector<int>     *mu_nTRTHits;
   vector<int>     *mu_nTRTHighTHits;
   vector<int>     *mu_nBLSharedHits;
   vector<int>     *mu_nPixSharedHits;
   vector<int>     *mu_nPixHoles;
   vector<int>     *mu_nSCTSharedHits;
   vector<int>     *mu_nSCTHoles;
   vector<int>     *mu_nTRTOutliers;
   vector<int>     *mu_nTRTHighTOutliers;
   vector<int>     *mu_nGangedPixels;
   vector<int>     *mu_nPixelDeadSensors;
   vector<int>     *mu_nSCTDeadSensors;
   vector<int>     *mu_nTRTDeadStraws;
   vector<int>     *mu_expectBLayerHit;
   vector<int>     *mu_nPrecisionLayers;
   vector<int>     *mu_nPrecisionHoleLayers;
   vector<int>     *mu_nPhiLayers;
   vector<int>     *mu_nPhiHoleLayers;
   vector<int>     *mu_nTrigEtaLayers;
   vector<int>     *mu_nTrigEtaHoleLayers;
   vector<int>     *mu_primarySector;
   vector<int>     *mu_secondarySector;
   vector<int>     *mu_nInnerSmallHits;
   vector<int>     *mu_nInnerLargeHits;
   vector<int>     *mu_nMiddleSmallHits;
   vector<int>     *mu_nMiddleLargeHits;
   vector<int>     *mu_nOuterSmallHits;
   vector<int>     *mu_nOuterLargeHits;
   vector<int>     *mu_nExtendedSmallHits;
   vector<int>     *mu_nExtendedLargeHits;
   vector<int>     *mu_nInnerSmallHoles;
   vector<int>     *mu_nInnerLargeHoles;
   vector<int>     *mu_nMiddleSmallHoles;
   vector<int>     *mu_nMiddleLargeHoles;
   vector<int>     *mu_nOuterSmallHoles;
   vector<int>     *mu_nOuterLargeHoles;
   vector<int>     *mu_nExtendedSmallHoles;
   vector<int>     *mu_nExtendedLargeHoles;
   vector<int>     *mu_nPhiLayer1Hits;
   vector<int>     *mu_nPhiLayer2Hits;
   vector<int>     *mu_nPhiLayer3Hits;
   vector<int>     *mu_nPhiLayer4Hits;
   vector<int>     *mu_nEtaLayer1Hits;
   vector<int>     *mu_nEtaLayer2Hits;
   vector<int>     *mu_nEtaLayer3Hits;
   vector<int>     *mu_nEtaLayer4Hits;
   vector<int>     *mu_nPhiLayer1Holes;
   vector<int>     *mu_nPhiLayer2Holes;
   vector<int>     *mu_nPhiLayer3Holes;
   vector<int>     *mu_nPhiLayer4Holes;
   vector<int>     *mu_nEtaLayer1Holes;
   vector<int>     *mu_nEtaLayer2Holes;
   vector<int>     *mu_nEtaLayer3Holes;
   vector<int>     *mu_nEtaLayer4Holes;
   vector<float>   *museg_x;
   vector<float>   *museg_y;
   vector<float>   *museg_z;
   vector<float>   *museg_px;
   vector<float>   *museg_py;
   vector<float>   *museg_pz;
   vector<float>   *museg_t0;
   vector<float>   *museg_t0error;
   vector<float>   *museg_chi2;
   vector<float>   *museg_ndof;
   vector<int>     *museg_sector;
   vector<int>     *museg_stationName;
   vector<int>     *museg_stationEta;
   vector<int>     *ext_mu_bias_type;
   vector<int>     *ext_mu_bias_index;
   vector<int>     *ext_mu_bias_size;
   vector<vector<int> > *ext_mu_bias_targetVec;
   vector<vector<float> > *ext_mu_bias_targetDistanceVec;
   vector<vector<float> > *ext_mu_bias_targetEtaVec;
   vector<vector<float> > *ext_mu_bias_targetPhiVec;
   vector<vector<float> > *ext_mu_bias_targetDeltaEtaVec;
   vector<vector<float> > *ext_mu_bias_targetDeltaPhiVec;
   vector<vector<float> > *ext_mu_bias_targetPxVec;
   vector<vector<float> > *ext_mu_bias_targetPyVec;
   vector<vector<float> > *ext_mu_bias_targetPzVec;
   vector<unsigned int> *muctpi_dataWords;
   vector<float>   *muctpi_dw_eta;
   vector<float>   *muctpi_dw_phi;
   vector<short>   *muctpi_dw_source;
   vector<short>   *muctpi_dw_hemisphere;
   vector<short>   *muctpi_dw_bcid;
   vector<short>   *muctpi_dw_sectorID;
   vector<short>   *muctpi_dw_thrNumber;
   vector<short>   *muctpi_dw_roi;
   vector<short>   *muctpi_dw_veto;
   vector<short>   *muctpi_dw_firstCandidate;
   vector<short>   *muctpi_dw_moreCandInRoI;
   vector<short>   *muctpi_dw_moreCandInSector;
   vector<short>   *muctpi_dw_charge;
   vector<short>   *muctpi_dw_candidateVetoed;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_detmask0;   //!
   TBranch        *b_detmask1;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_fwdFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_pixelError;   //!
   TBranch        *b_sctError;   //!
   TBranch        *b_trtError;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_muonError;   //!
   TBranch        *b_fwdError;   //!
   TBranch        *b_coreError;   //!
   TBranch        *b_vxp_x;   //!
   TBranch        *b_vxp_y;   //!
   TBranch        *b_vxp_z;   //!
   TBranch        *b_vxp_chi2;   //!
   TBranch        *b_vxp_ndof;   //!
   TBranch        *b_vxp_nTracks;   //!
   TBranch        *b_TGC_prd_x;   //!
   TBranch        *b_TGC_prd_y;   //!
   TBranch        *b_TGC_prd_z;   //!
   TBranch        *b_TGC_prd_shortWidth;   //!
   TBranch        *b_TGC_prd_longWidth;   //!
   TBranch        *b_TGC_prd_length;   //!
   TBranch        *b_TGC_prd_isStrip;   //!
   TBranch        *b_TGC_prd_gasGap;   //!
   TBranch        *b_TGC_prd_channel;   //!
   TBranch        *b_TGC_prd_eta;   //!
   TBranch        *b_TGC_prd_phi;   //!
   TBranch        *b_TGC_prd_station;   //!
   TBranch        *b_TGC_prd_bunch;   //!
   TBranch        *b_trig_L1_mu_eta;   //!
   TBranch        *b_trig_L1_mu_phi;   //!
   TBranch        *b_trig_L1_mu_thrName;   //!
   TBranch        *b_trig_L1_mu_thrNumber;   //!
   TBranch        *b_trig_L1_mu_RoINumber;   //!
   TBranch        *b_trig_L1_mu_sectorAddress;   //!
   TBranch        *b_trig_L1_mu_firstCandidate;   //!
   TBranch        *b_trig_L1_mu_moreCandInRoI;   //!
   TBranch        *b_trig_L1_mu_moreCandInSector;   //!
   TBranch        *b_trig_L1_mu_source;   //!
   TBranch        *b_trig_L1_mu_hemisphere;   //!
   TBranch        *b_trig_L1_mu_charge;   //!
   TBranch        *b_trig_L1_mu_vetoed;   //!
   TBranch        *b_trigger_info_chain;   //!
   TBranch        *b_trigger_info_isPassed;   //!
   TBranch        *b_trigger_info_typeVec;   //!
   TBranch        *b_trigger_info_ptVec;   //!
   TBranch        *b_trigger_info_etaVec;   //!
   TBranch        *b_trigger_info_phiVec;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_m;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_author;   //!
   TBranch        *b_mu_allAuthors;   //!
   TBranch        *b_mu_muonType;   //!
   TBranch        *b_mu_etcone20;   //!
   TBranch        *b_mu_etcone30;   //!
   TBranch        *b_mu_etcone40;   //!
   TBranch        *b_mu_ptcone20;   //!
   TBranch        *b_mu_ptcone30;   //!
   TBranch        *b_mu_ptcone40;   //!
   TBranch        *b_mu_trackfitchi2;   //!
   TBranch        *b_mu_trackfitndof;   //!
   TBranch        *b_mu_msInnerMatchChi2;   //!
   TBranch        *b_mu_msOuterMatchChi2;   //!
   TBranch        *b_mu_msInnerMatchDOF;   //!
   TBranch        *b_mu_msOuterMatchDOF;   //!
   TBranch        *b_mu_nOutliersOnTrack;   //!
   TBranch        *b_mu_nBLHits;   //!
   TBranch        *b_mu_nPixHits;   //!
   TBranch        *b_mu_nSCTHits;   //!
   TBranch        *b_mu_nTRTHits;   //!
   TBranch        *b_mu_nTRTHighTHits;   //!
   TBranch        *b_mu_nBLSharedHits;   //!
   TBranch        *b_mu_nPixSharedHits;   //!
   TBranch        *b_mu_nPixHoles;   //!
   TBranch        *b_mu_nSCTSharedHits;   //!
   TBranch        *b_mu_nSCTHoles;   //!
   TBranch        *b_mu_nTRTOutliers;   //!
   TBranch        *b_mu_nTRTHighTOutliers;   //!
   TBranch        *b_mu_nGangedPixels;   //!
   TBranch        *b_mu_nPixelDeadSensors;   //!
   TBranch        *b_mu_nSCTDeadSensors;   //!
   TBranch        *b_mu_nTRTDeadStraws;   //!
   TBranch        *b_mu_expectBLayerHit;   //!
   TBranch        *b_mu_nPrecisionLayers;   //!
   TBranch        *b_mu_nPrecisionHoleLayers;   //!
   TBranch        *b_mu_nPhiLayers;   //!
   TBranch        *b_mu_nPhiHoleLayers;   //!
   TBranch        *b_mu_nTrigEtaLayers;   //!
   TBranch        *b_mu_nTrigEtaHoleLayers;   //!
   TBranch        *b_mu_primarySector;   //!
   TBranch        *b_mu_secondarySector;   //!
   TBranch        *b_mu_nInnerSmallHits;   //!
   TBranch        *b_mu_nInnerLargeHits;   //!
   TBranch        *b_mu_nMiddleSmallHits;   //!
   TBranch        *b_mu_nMiddleLargeHits;   //!
   TBranch        *b_mu_nOuterSmallHits;   //!
   TBranch        *b_mu_nOuterLargeHits;   //!
   TBranch        *b_mu_nExtendedSmallHits;   //!
   TBranch        *b_mu_nExtendedLargeHits;   //!
   TBranch        *b_mu_nInnerSmallHoles;   //!
   TBranch        *b_mu_nInnerLargeHoles;   //!
   TBranch        *b_mu_nMiddleSmallHoles;   //!
   TBranch        *b_mu_nMiddleLargeHoles;   //!
   TBranch        *b_mu_nOuterSmallHoles;   //!
   TBranch        *b_mu_nOuterLargeHoles;   //!
   TBranch        *b_mu_nExtendedSmallHoles;   //!
   TBranch        *b_mu_nExtendedLargeHoles;   //!
   TBranch        *b_mu_nPhiLayer1Hits;   //!
   TBranch        *b_mu_nPhiLayer2Hits;   //!
   TBranch        *b_mu_nPhiLayer3Hits;   //!
   TBranch        *b_mu_nPhiLayer4Hits;   //!
   TBranch        *b_mu_nEtaLayer1Hits;   //!
   TBranch        *b_mu_nEtaLayer2Hits;   //!
   TBranch        *b_mu_nEtaLayer3Hits;   //!
   TBranch        *b_mu_nEtaLayer4Hits;   //!
   TBranch        *b_mu_nPhiLayer1Holes;   //!
   TBranch        *b_mu_nPhiLayer2Holes;   //!
   TBranch        *b_mu_nPhiLayer3Holes;   //!
   TBranch        *b_mu_nPhiLayer4Holes;   //!
   TBranch        *b_mu_nEtaLayer1Holes;   //!
   TBranch        *b_mu_nEtaLayer2Holes;   //!
   TBranch        *b_mu_nEtaLayer3Holes;   //!
   TBranch        *b_mu_nEtaLayer4Holes;   //!
   TBranch        *b_museg_x;   //!
   TBranch        *b_museg_y;   //!
   TBranch        *b_museg_z;   //!
   TBranch        *b_museg_px;   //!
   TBranch        *b_museg_py;   //!
   TBranch        *b_museg_pz;   //!
   TBranch        *b_museg_t0;   //!
   TBranch        *b_museg_t0error;   //!
   TBranch        *b_museg_chi2;   //!
   TBranch        *b_museg_ndof;   //!
   TBranch        *b_museg_sector;   //!
   TBranch        *b_museg_stationName;   //!
   TBranch        *b_museg_stationEta;   //!
   TBranch        *b_ext_mu_bias_type;   //!
   TBranch        *b_ext_mu_bias_index;   //!
   TBranch        *b_ext_mu_bias_size;   //!
   TBranch        *b_ext_mu_bias_targetVec;   //!
   TBranch        *b_ext_mu_bias_targetDistanceVec;   //!
   TBranch        *b_ext_mu_bias_targetEtaVec;   //!
   TBranch        *b_ext_mu_bias_targetPhiVec;   //!
   TBranch        *b_ext_mu_bias_targetDeltaEtaVec;   //!
   TBranch        *b_ext_mu_bias_targetDeltaPhiVec;   //!
   TBranch        *b_ext_mu_bias_targetPxVec;   //!
   TBranch        *b_ext_mu_bias_targetPyVec;   //!
   TBranch        *b_ext_mu_bias_targetPzVec;   //!
   TBranch        *b_muctpi_dataWords;   //!
   TBranch        *b_muctpi_dw_eta;   //!
   TBranch        *b_muctpi_dw_phi;   //!
   TBranch        *b_muctpi_dw_source;   //!
   TBranch        *b_muctpi_dw_hemisphere;   //!
   TBranch        *b_muctpi_dw_bcid;   //!
   TBranch        *b_muctpi_dw_sectorID;   //!
   TBranch        *b_muctpi_dw_thrNumber;   //!
   TBranch        *b_muctpi_dw_roi;   //!
   TBranch        *b_muctpi_dw_veto;   //!
   TBranch        *b_muctpi_dw_firstCandidate;   //!
   TBranch        *b_muctpi_dw_moreCandInRoI;   //!
   TBranch        *b_muctpi_dw_moreCandInSector;   //!
   TBranch        *b_muctpi_dw_charge;   //!
   TBranch        *b_muctpi_dw_candidateVetoed;   //!

   getprd(TTree *tree=0);
   virtual ~getprd();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef getprd_cxx
getprd::getprd(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("physics",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("physics","");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348197.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348251.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348354.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348495.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348511.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348609.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348610.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348618.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348836.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.00348403.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/Zmumu_Sample/ZmumuSample_original_tuning/user.teramura.mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.22.0.14.tuning_F_LightTGCNtuple/*.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/Zmumu_Sample/ZmumuSample_original_tuning/user.teramura.mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.22.0.14.orignal_F_LightTGCNtuple/*5.root/physics");
      //chain->Add("/gpfs/fs2001/teramura/Zmumu_Sample/ZmumuSample_original_tuning/user.teramura.mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.22.0.14.orignal_F_LightTGCNtuple/*6.root/physics");
      chain->Add("/gpfs/fs2001/teramura/master/user.teramura.data15_13TeV.00267073.physics_Main.merge.DESDM_MCP.r12746_p3833_p3833_LightTGCNtuple/*.root/physics");
      chain->Add("/gpfs/fs2001/teramura/master/user.teramura.data15_13TeV.00267073.physics_Main.merge.r12746_p3833_LightTGCNtuple/*.root/physics");
      chain->Add("/gpfs/fs2001/teramura/master/user.teramura.data15_13TeV.00267073.physics_Main.recon.DESDM_MCP.r12746_LightTGCNtuple/*.root/physics");

            //chain->Add("/home/teramura/Run2data/periodB/add.*.root/physics");
      //chain->Add("/home/teramura/Run2data/periodA/analysis/data/add.mc.root/physics");
      //chain->Add("/home/teramura/Run2data/MC/user.teramura.Ntuple.424008.Singlemuon.Pt20.Run2.v3_EXT0_LightTGCNtuple/add.22550265.root/physics");
	//chain->Add("/home/teramura/Run2data/periodA/analysis/data/LightTGCNtuple_shiomi.root/physics");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

getprd::~getprd()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t getprd::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t getprd::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void getprd::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   vxp_x = 0;
   vxp_y = 0;
   vxp_z = 0;
   vxp_chi2 = 0;
   vxp_ndof = 0;
   vxp_nTracks = 0;
   TGC_prd_x = 0;
   TGC_prd_y = 0;
   TGC_prd_z = 0;
   TGC_prd_shortWidth = 0;
   TGC_prd_longWidth = 0;
   TGC_prd_length = 0;
   TGC_prd_isStrip = 0;
   TGC_prd_gasGap = 0;
   TGC_prd_channel = 0;
   TGC_prd_eta = 0;
   TGC_prd_phi = 0;
   TGC_prd_station = 0;
   TGC_prd_bunch = 0;
   trig_L1_mu_eta = 0;
   trig_L1_mu_phi = 0;
   trig_L1_mu_thrName = 0;
   trig_L1_mu_thrNumber = 0;
   trig_L1_mu_RoINumber = 0;
   trig_L1_mu_sectorAddress = 0;
   trig_L1_mu_firstCandidate = 0;
   trig_L1_mu_moreCandInRoI = 0;
   trig_L1_mu_moreCandInSector = 0;
   trig_L1_mu_source = 0;
   trig_L1_mu_hemisphere = 0;
   trig_L1_mu_charge = 0;
   trig_L1_mu_vetoed = 0;
   trigger_info_chain = 0;
   trigger_info_isPassed = 0;
   trigger_info_typeVec = 0;
   trigger_info_ptVec = 0;
   trigger_info_etaVec = 0;
   trigger_info_phiVec = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_m = 0;
   mu_charge = 0;
   mu_author = 0;
   mu_allAuthors = 0;
   mu_muonType = 0;
   mu_etcone20 = 0;
   mu_etcone30 = 0;
   mu_etcone40 = 0;
   mu_ptcone20 = 0;
   mu_ptcone30 = 0;
   mu_ptcone40 = 0;
   mu_trackfitchi2 = 0;
   mu_trackfitndof = 0;
   mu_msInnerMatchChi2 = 0;
   mu_msOuterMatchChi2 = 0;
   mu_msInnerMatchDOF = 0;
   mu_msOuterMatchDOF = 0;
   mu_nOutliersOnTrack = 0;
   mu_nBLHits = 0;
   mu_nPixHits = 0;
   mu_nSCTHits = 0;
   mu_nTRTHits = 0;
   mu_nTRTHighTHits = 0;
   mu_nBLSharedHits = 0;
   mu_nPixSharedHits = 0;
   mu_nPixHoles = 0;
   mu_nSCTSharedHits = 0;
   mu_nSCTHoles = 0;
   mu_nTRTOutliers = 0;
   mu_nTRTHighTOutliers = 0;
   mu_nGangedPixels = 0;
   mu_nPixelDeadSensors = 0;
   mu_nSCTDeadSensors = 0;
   mu_nTRTDeadStraws = 0;
   mu_expectBLayerHit = 0;
   mu_nPrecisionLayers = 0;
   mu_nPrecisionHoleLayers = 0;
   mu_nPhiLayers = 0;
   mu_nPhiHoleLayers = 0;
   mu_nTrigEtaLayers = 0;
   mu_nTrigEtaHoleLayers = 0;
   mu_primarySector = 0;
   mu_secondarySector = 0;
   mu_nInnerSmallHits = 0;
   mu_nInnerLargeHits = 0;
   mu_nMiddleSmallHits = 0;
   mu_nMiddleLargeHits = 0;
   mu_nOuterSmallHits = 0;
   mu_nOuterLargeHits = 0;
   mu_nExtendedSmallHits = 0;
   mu_nExtendedLargeHits = 0;
   mu_nInnerSmallHoles = 0;
   mu_nInnerLargeHoles = 0;
   mu_nMiddleSmallHoles = 0;
   mu_nMiddleLargeHoles = 0;
   mu_nOuterSmallHoles = 0;
   mu_nOuterLargeHoles = 0;
   mu_nExtendedSmallHoles = 0;
   mu_nExtendedLargeHoles = 0;
   mu_nPhiLayer1Hits = 0;
   mu_nPhiLayer2Hits = 0;
   mu_nPhiLayer3Hits = 0;
   mu_nPhiLayer4Hits = 0;
   mu_nEtaLayer1Hits = 0;
   mu_nEtaLayer2Hits = 0;
   mu_nEtaLayer3Hits = 0;
   mu_nEtaLayer4Hits = 0;
   mu_nPhiLayer1Holes = 0;
   mu_nPhiLayer2Holes = 0;
   mu_nPhiLayer3Holes = 0;
   mu_nPhiLayer4Holes = 0;
   mu_nEtaLayer1Holes = 0;
   mu_nEtaLayer2Holes = 0;
   mu_nEtaLayer3Holes = 0;
   mu_nEtaLayer4Holes = 0;
   museg_x = 0;
   museg_y = 0;
   museg_z = 0;
   museg_px = 0;
   museg_py = 0;
   museg_pz = 0;
   museg_t0 = 0;
   museg_t0error = 0;
   museg_chi2 = 0;
   museg_ndof = 0;
   museg_sector = 0;
   museg_stationName = 0;
   museg_stationEta = 0;
   ext_mu_bias_type = 0;
   ext_mu_bias_index = 0;
   ext_mu_bias_size = 0;
   ext_mu_bias_targetVec = 0;
   ext_mu_bias_targetDistanceVec = 0;
   ext_mu_bias_targetEtaVec = 0;
   ext_mu_bias_targetPhiVec = 0;
   ext_mu_bias_targetDeltaEtaVec = 0;
   ext_mu_bias_targetDeltaPhiVec = 0;
   ext_mu_bias_targetPxVec = 0;
   ext_mu_bias_targetPyVec = 0;
   ext_mu_bias_targetPzVec = 0;
   muctpi_dw_eta = 0;
   muctpi_dw_phi = 0;
   muctpi_dw_source = 0;
   muctpi_dw_hemisphere = 0;
   muctpi_dw_bcid = 0;
   muctpi_dw_sectorID = 0;
   muctpi_dw_thrNumber = 0;
   muctpi_dw_roi = 0;
   muctpi_dw_veto = 0;
   muctpi_dw_firstCandidate = 0;
   muctpi_dw_moreCandInRoI = 0;
   muctpi_dw_moreCandInSector = 0;
   muctpi_dw_charge = 0;
   muctpi_dw_candidateVetoed = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_runNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
   fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
   fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
   fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
   fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
   fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
   fChain->SetBranchAddress("vxp_x", &vxp_x, &b_vxp_x);
   fChain->SetBranchAddress("vxp_y", &vxp_y, &b_vxp_y);
   fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
   fChain->SetBranchAddress("vxp_chi2", &vxp_chi2, &b_vxp_chi2);
   fChain->SetBranchAddress("vxp_ndof", &vxp_ndof, &b_vxp_ndof);
   fChain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
   fChain->SetBranchAddress("TGC_prd_x", &TGC_prd_x, &b_TGC_prd_x);
   fChain->SetBranchAddress("TGC_prd_y", &TGC_prd_y, &b_TGC_prd_y);
   fChain->SetBranchAddress("TGC_prd_z", &TGC_prd_z, &b_TGC_prd_z);
   fChain->SetBranchAddress("TGC_prd_shortWidth", &TGC_prd_shortWidth, &b_TGC_prd_shortWidth);
   fChain->SetBranchAddress("TGC_prd_longWidth", &TGC_prd_longWidth, &b_TGC_prd_longWidth);
   fChain->SetBranchAddress("TGC_prd_length", &TGC_prd_length, &b_TGC_prd_length);
   fChain->SetBranchAddress("TGC_prd_isStrip", &TGC_prd_isStrip, &b_TGC_prd_isStrip);
   fChain->SetBranchAddress("TGC_prd_gasGap", &TGC_prd_gasGap, &b_TGC_prd_gasGap);
   fChain->SetBranchAddress("TGC_prd_channel", &TGC_prd_channel, &b_TGC_prd_channel);
   fChain->SetBranchAddress("TGC_prd_eta", &TGC_prd_eta, &b_TGC_prd_eta);
   fChain->SetBranchAddress("TGC_prd_phi", &TGC_prd_phi, &b_TGC_prd_phi);
   fChain->SetBranchAddress("TGC_prd_station", &TGC_prd_station, &b_TGC_prd_station);
   fChain->SetBranchAddress("TGC_prd_bunch", &TGC_prd_bunch, &b_TGC_prd_bunch);
   fChain->SetBranchAddress("trig_L1_mu_eta", &trig_L1_mu_eta, &b_trig_L1_mu_eta);
   fChain->SetBranchAddress("trig_L1_mu_phi", &trig_L1_mu_phi, &b_trig_L1_mu_phi);
   fChain->SetBranchAddress("trig_L1_mu_thrName", &trig_L1_mu_thrName, &b_trig_L1_mu_thrName);
   fChain->SetBranchAddress("trig_L1_mu_thrNumber", &trig_L1_mu_thrNumber, &b_trig_L1_mu_thrNumber);
   fChain->SetBranchAddress("trig_L1_mu_RoINumber", &trig_L1_mu_RoINumber, &b_trig_L1_mu_RoINumber);
   fChain->SetBranchAddress("trig_L1_mu_sectorAddress", &trig_L1_mu_sectorAddress, &b_trig_L1_mu_sectorAddress);
   fChain->SetBranchAddress("trig_L1_mu_firstCandidate", &trig_L1_mu_firstCandidate, &b_trig_L1_mu_firstCandidate);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInRoI", &trig_L1_mu_moreCandInRoI, &b_trig_L1_mu_moreCandInRoI);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInSector", &trig_L1_mu_moreCandInSector, &b_trig_L1_mu_moreCandInSector);
   fChain->SetBranchAddress("trig_L1_mu_source", &trig_L1_mu_source, &b_trig_L1_mu_source);
   fChain->SetBranchAddress("trig_L1_mu_hemisphere", &trig_L1_mu_hemisphere, &b_trig_L1_mu_hemisphere);
   fChain->SetBranchAddress("trig_L1_mu_charge", &trig_L1_mu_charge, &b_trig_L1_mu_charge);
   fChain->SetBranchAddress("trig_L1_mu_vetoed", &trig_L1_mu_vetoed, &b_trig_L1_mu_vetoed);
   fChain->SetBranchAddress("trigger_info_chain", &trigger_info_chain, &b_trigger_info_chain);
   fChain->SetBranchAddress("trigger_info_isPassed", &trigger_info_isPassed, &b_trigger_info_isPassed);
   fChain->SetBranchAddress("trigger_info_typeVec", &trigger_info_typeVec, &b_trigger_info_typeVec);
   fChain->SetBranchAddress("trigger_info_ptVec", &trigger_info_ptVec, &b_trigger_info_ptVec);
   fChain->SetBranchAddress("trigger_info_etaVec", &trigger_info_etaVec, &b_trigger_info_etaVec);
   fChain->SetBranchAddress("trigger_info_phiVec", &trigger_info_phiVec, &b_trigger_info_phiVec);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
   fChain->SetBranchAddress("mu_allAuthors", &mu_allAuthors, &b_mu_allAuthors);
   fChain->SetBranchAddress("mu_muonType", &mu_muonType, &b_mu_muonType);
   fChain->SetBranchAddress("mu_etcone20", &mu_etcone20, &b_mu_etcone20);
   fChain->SetBranchAddress("mu_etcone30", &mu_etcone30, &b_mu_etcone30);
   fChain->SetBranchAddress("mu_etcone40", &mu_etcone40, &b_mu_etcone40);
   fChain->SetBranchAddress("mu_ptcone20", &mu_ptcone20, &b_mu_ptcone20);
   fChain->SetBranchAddress("mu_ptcone30", &mu_ptcone30, &b_mu_ptcone30);
   fChain->SetBranchAddress("mu_ptcone40", &mu_ptcone40, &b_mu_ptcone40);
   fChain->SetBranchAddress("mu_trackfitchi2", &mu_trackfitchi2, &b_mu_trackfitchi2);
   fChain->SetBranchAddress("mu_trackfitndof", &mu_trackfitndof, &b_mu_trackfitndof);
   fChain->SetBranchAddress("mu_msInnerMatchChi2", &mu_msInnerMatchChi2, &b_mu_msInnerMatchChi2);
   fChain->SetBranchAddress("mu_msOuterMatchChi2", &mu_msOuterMatchChi2, &b_mu_msOuterMatchChi2);
   fChain->SetBranchAddress("mu_msInnerMatchDOF", &mu_msInnerMatchDOF, &b_mu_msInnerMatchDOF);
   fChain->SetBranchAddress("mu_msOuterMatchDOF", &mu_msOuterMatchDOF, &b_mu_msOuterMatchDOF);
   fChain->SetBranchAddress("mu_nOutliersOnTrack", &mu_nOutliersOnTrack, &b_mu_nOutliersOnTrack);
   fChain->SetBranchAddress("mu_nBLHits", &mu_nBLHits, &b_mu_nBLHits);
   fChain->SetBranchAddress("mu_nPixHits", &mu_nPixHits, &b_mu_nPixHits);
   fChain->SetBranchAddress("mu_nSCTHits", &mu_nSCTHits, &b_mu_nSCTHits);
   fChain->SetBranchAddress("mu_nTRTHits", &mu_nTRTHits, &b_mu_nTRTHits);
   fChain->SetBranchAddress("mu_nTRTHighTHits", &mu_nTRTHighTHits, &b_mu_nTRTHighTHits);
   fChain->SetBranchAddress("mu_nBLSharedHits", &mu_nBLSharedHits, &b_mu_nBLSharedHits);
   fChain->SetBranchAddress("mu_nPixSharedHits", &mu_nPixSharedHits, &b_mu_nPixSharedHits);
   fChain->SetBranchAddress("mu_nPixHoles", &mu_nPixHoles, &b_mu_nPixHoles);
   fChain->SetBranchAddress("mu_nSCTSharedHits", &mu_nSCTSharedHits, &b_mu_nSCTSharedHits);
   fChain->SetBranchAddress("mu_nSCTHoles", &mu_nSCTHoles, &b_mu_nSCTHoles);
   fChain->SetBranchAddress("mu_nTRTOutliers", &mu_nTRTOutliers, &b_mu_nTRTOutliers);
   fChain->SetBranchAddress("mu_nTRTHighTOutliers", &mu_nTRTHighTOutliers, &b_mu_nTRTHighTOutliers);
   fChain->SetBranchAddress("mu_nGangedPixels", &mu_nGangedPixels, &b_mu_nGangedPixels);
   fChain->SetBranchAddress("mu_nPixelDeadSensors", &mu_nPixelDeadSensors, &b_mu_nPixelDeadSensors);
   fChain->SetBranchAddress("mu_nSCTDeadSensors", &mu_nSCTDeadSensors, &b_mu_nSCTDeadSensors);
   fChain->SetBranchAddress("mu_nTRTDeadStraws", &mu_nTRTDeadStraws, &b_mu_nTRTDeadStraws);
   fChain->SetBranchAddress("mu_expectBLayerHit", &mu_expectBLayerHit, &b_mu_expectBLayerHit);
   fChain->SetBranchAddress("mu_nPrecisionLayers", &mu_nPrecisionLayers, &b_mu_nPrecisionLayers);
   fChain->SetBranchAddress("mu_nPrecisionHoleLayers", &mu_nPrecisionHoleLayers, &b_mu_nPrecisionHoleLayers);
   fChain->SetBranchAddress("mu_nPhiLayers", &mu_nPhiLayers, &b_mu_nPhiLayers);
   fChain->SetBranchAddress("mu_nPhiHoleLayers", &mu_nPhiHoleLayers, &b_mu_nPhiHoleLayers);
   fChain->SetBranchAddress("mu_nTrigEtaLayers", &mu_nTrigEtaLayers, &b_mu_nTrigEtaLayers);
   fChain->SetBranchAddress("mu_nTrigEtaHoleLayers", &mu_nTrigEtaHoleLayers, &b_mu_nTrigEtaHoleLayers);
   fChain->SetBranchAddress("mu_primarySector", &mu_primarySector, &b_mu_primarySector);
   fChain->SetBranchAddress("mu_secondarySector", &mu_secondarySector, &b_mu_secondarySector);
   fChain->SetBranchAddress("mu_nInnerSmallHits", &mu_nInnerSmallHits, &b_mu_nInnerSmallHits);
   fChain->SetBranchAddress("mu_nInnerLargeHits", &mu_nInnerLargeHits, &b_mu_nInnerLargeHits);
   fChain->SetBranchAddress("mu_nMiddleSmallHits", &mu_nMiddleSmallHits, &b_mu_nMiddleSmallHits);
   fChain->SetBranchAddress("mu_nMiddleLargeHits", &mu_nMiddleLargeHits, &b_mu_nMiddleLargeHits);
   fChain->SetBranchAddress("mu_nOuterSmallHits", &mu_nOuterSmallHits, &b_mu_nOuterSmallHits);
   fChain->SetBranchAddress("mu_nOuterLargeHits", &mu_nOuterLargeHits, &b_mu_nOuterLargeHits);
   fChain->SetBranchAddress("mu_nExtendedSmallHits", &mu_nExtendedSmallHits, &b_mu_nExtendedSmallHits);
   fChain->SetBranchAddress("mu_nExtendedLargeHits", &mu_nExtendedLargeHits, &b_mu_nExtendedLargeHits);
   fChain->SetBranchAddress("mu_nInnerSmallHoles", &mu_nInnerSmallHoles, &b_mu_nInnerSmallHoles);
   fChain->SetBranchAddress("mu_nInnerLargeHoles", &mu_nInnerLargeHoles, &b_mu_nInnerLargeHoles);
   fChain->SetBranchAddress("mu_nMiddleSmallHoles", &mu_nMiddleSmallHoles, &b_mu_nMiddleSmallHoles);
   fChain->SetBranchAddress("mu_nMiddleLargeHoles", &mu_nMiddleLargeHoles, &b_mu_nMiddleLargeHoles);
   fChain->SetBranchAddress("mu_nOuterSmallHoles", &mu_nOuterSmallHoles, &b_mu_nOuterSmallHoles);
   fChain->SetBranchAddress("mu_nOuterLargeHoles", &mu_nOuterLargeHoles, &b_mu_nOuterLargeHoles);
   fChain->SetBranchAddress("mu_nExtendedSmallHoles", &mu_nExtendedSmallHoles, &b_mu_nExtendedSmallHoles);
   fChain->SetBranchAddress("mu_nExtendedLargeHoles", &mu_nExtendedLargeHoles, &b_mu_nExtendedLargeHoles);
   fChain->SetBranchAddress("mu_nPhiLayer1Hits", &mu_nPhiLayer1Hits, &b_mu_nPhiLayer1Hits);
   fChain->SetBranchAddress("mu_nPhiLayer2Hits", &mu_nPhiLayer2Hits, &b_mu_nPhiLayer2Hits);
   fChain->SetBranchAddress("mu_nPhiLayer3Hits", &mu_nPhiLayer3Hits, &b_mu_nPhiLayer3Hits);
   fChain->SetBranchAddress("mu_nPhiLayer4Hits", &mu_nPhiLayer4Hits, &b_mu_nPhiLayer4Hits);
   fChain->SetBranchAddress("mu_nEtaLayer1Hits", &mu_nEtaLayer1Hits, &b_mu_nEtaLayer1Hits);
   fChain->SetBranchAddress("mu_nEtaLayer2Hits", &mu_nEtaLayer2Hits, &b_mu_nEtaLayer2Hits);
   fChain->SetBranchAddress("mu_nEtaLayer3Hits", &mu_nEtaLayer3Hits, &b_mu_nEtaLayer3Hits);
   fChain->SetBranchAddress("mu_nEtaLayer4Hits", &mu_nEtaLayer4Hits, &b_mu_nEtaLayer4Hits);
   fChain->SetBranchAddress("mu_nPhiLayer1Holes", &mu_nPhiLayer1Holes, &b_mu_nPhiLayer1Holes);
   fChain->SetBranchAddress("mu_nPhiLayer2Holes", &mu_nPhiLayer2Holes, &b_mu_nPhiLayer2Holes);
   fChain->SetBranchAddress("mu_nPhiLayer3Holes", &mu_nPhiLayer3Holes, &b_mu_nPhiLayer3Holes);
   fChain->SetBranchAddress("mu_nPhiLayer4Holes", &mu_nPhiLayer4Holes, &b_mu_nPhiLayer4Holes);
   fChain->SetBranchAddress("mu_nEtaLayer1Holes", &mu_nEtaLayer1Holes, &b_mu_nEtaLayer1Holes);
   fChain->SetBranchAddress("mu_nEtaLayer2Holes", &mu_nEtaLayer2Holes, &b_mu_nEtaLayer2Holes);
   fChain->SetBranchAddress("mu_nEtaLayer3Holes", &mu_nEtaLayer3Holes, &b_mu_nEtaLayer3Holes);
   fChain->SetBranchAddress("mu_nEtaLayer4Holes", &mu_nEtaLayer4Holes, &b_mu_nEtaLayer4Holes);
   fChain->SetBranchAddress("museg_x", &museg_x, &b_museg_x);
   fChain->SetBranchAddress("museg_y", &museg_y, &b_museg_y);
   fChain->SetBranchAddress("museg_z", &museg_z, &b_museg_z);
   fChain->SetBranchAddress("museg_px", &museg_px, &b_museg_px);
   fChain->SetBranchAddress("museg_py", &museg_py, &b_museg_py);
   fChain->SetBranchAddress("museg_pz", &museg_pz, &b_museg_pz);
   fChain->SetBranchAddress("museg_t0", &museg_t0, &b_museg_t0);
   fChain->SetBranchAddress("museg_t0error", &museg_t0error, &b_museg_t0error);
   fChain->SetBranchAddress("museg_chi2", &museg_chi2, &b_museg_chi2);
   fChain->SetBranchAddress("museg_ndof", &museg_ndof, &b_museg_ndof);
   fChain->SetBranchAddress("museg_sector", &museg_sector, &b_museg_sector);
   fChain->SetBranchAddress("museg_stationName", &museg_stationName, &b_museg_stationName);
   fChain->SetBranchAddress("museg_stationEta", &museg_stationEta, &b_museg_stationEta);
   fChain->SetBranchAddress("ext_mu_bias_type", &ext_mu_bias_type, &b_ext_mu_bias_type);
   fChain->SetBranchAddress("ext_mu_bias_index", &ext_mu_bias_index, &b_ext_mu_bias_index);
   fChain->SetBranchAddress("ext_mu_bias_size", &ext_mu_bias_size, &b_ext_mu_bias_size);
   fChain->SetBranchAddress("ext_mu_bias_targetVec", &ext_mu_bias_targetVec, &b_ext_mu_bias_targetVec);
   fChain->SetBranchAddress("ext_mu_bias_targetDistanceVec", &ext_mu_bias_targetDistanceVec, &b_ext_mu_bias_targetDistanceVec);
   fChain->SetBranchAddress("ext_mu_bias_targetEtaVec", &ext_mu_bias_targetEtaVec, &b_ext_mu_bias_targetEtaVec);
   fChain->SetBranchAddress("ext_mu_bias_targetPhiVec", &ext_mu_bias_targetPhiVec, &b_ext_mu_bias_targetPhiVec);
   fChain->SetBranchAddress("ext_mu_bias_targetDeltaEtaVec", &ext_mu_bias_targetDeltaEtaVec, &b_ext_mu_bias_targetDeltaEtaVec);
   fChain->SetBranchAddress("ext_mu_bias_targetDeltaPhiVec", &ext_mu_bias_targetDeltaPhiVec, &b_ext_mu_bias_targetDeltaPhiVec);
   fChain->SetBranchAddress("ext_mu_bias_targetPxVec", &ext_mu_bias_targetPxVec, &b_ext_mu_bias_targetPxVec);
   fChain->SetBranchAddress("ext_mu_bias_targetPyVec", &ext_mu_bias_targetPyVec, &b_ext_mu_bias_targetPyVec);
   fChain->SetBranchAddress("ext_mu_bias_targetPzVec", &ext_mu_bias_targetPzVec, &b_ext_mu_bias_targetPzVec);
   fChain->SetBranchAddress("muctpi_dataWords", &muctpi_dataWords, &b_muctpi_dataWords);
   fChain->SetBranchAddress("muctpi_dw_eta", &muctpi_dw_eta, &b_muctpi_dw_eta);
   fChain->SetBranchAddress("muctpi_dw_phi", &muctpi_dw_phi, &b_muctpi_dw_phi);
   fChain->SetBranchAddress("muctpi_dw_source", &muctpi_dw_source, &b_muctpi_dw_source);
   fChain->SetBranchAddress("muctpi_dw_hemisphere", &muctpi_dw_hemisphere, &b_muctpi_dw_hemisphere);
   fChain->SetBranchAddress("muctpi_dw_bcid", &muctpi_dw_bcid, &b_muctpi_dw_bcid);
   fChain->SetBranchAddress("muctpi_dw_sectorID", &muctpi_dw_sectorID, &b_muctpi_dw_sectorID);
   fChain->SetBranchAddress("muctpi_dw_thrNumber", &muctpi_dw_thrNumber, &b_muctpi_dw_thrNumber);
   fChain->SetBranchAddress("muctpi_dw_roi", &muctpi_dw_roi, &b_muctpi_dw_roi);
   fChain->SetBranchAddress("muctpi_dw_veto", &muctpi_dw_veto, &b_muctpi_dw_veto);
   fChain->SetBranchAddress("muctpi_dw_firstCandidate", &muctpi_dw_firstCandidate, &b_muctpi_dw_firstCandidate);
   fChain->SetBranchAddress("muctpi_dw_moreCandInRoI", &muctpi_dw_moreCandInRoI, &b_muctpi_dw_moreCandInRoI);
   fChain->SetBranchAddress("muctpi_dw_moreCandInSector", &muctpi_dw_moreCandInSector, &b_muctpi_dw_moreCandInSector);
   fChain->SetBranchAddress("muctpi_dw_charge", &muctpi_dw_charge, &b_muctpi_dw_charge);
   fChain->SetBranchAddress("muctpi_dw_candidateVetoed", &muctpi_dw_candidateVetoed, &b_muctpi_dw_candidateVetoed);
   Notify();
}

Bool_t getprd::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void getprd::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t getprd::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef getprd_cxx
